#tag Class
Protected Class GenWebPopupMenu
Inherits WebPopupMenu
	#tag Method, Flags = &h0
		Sub loadCleEtrangere(dB As PostgreSQLDatabase,  table_name As String, prefix As String , table_zone As String, table_zonePrimSelect As String)
		  
		  Self.DeleteAllRows
		  
		  Dim recordSet as RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM " + table_name + _
		  " WHERE " + prefix + "_nom = '" + table_zone  +"' "  + _
		  "    AND "  + prefix + "_tri > 0 " + _
		  " ORDER BY " + prefix + "_tri"  + " ASC "
		  recordSet =  dB.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      dim lang as string = Session.Header("Accept-Language")
		      if instr(lang, "fr")>0 then
		        Self.AddRow(recordSet.Field( prefix + "_desc_fr").StringValue)
		      Else
		        Self.AddRow(recordSet.Field(prefix + "_desc_en").StringValue)
		      End If
		      
		      // Mettre la valeur du tri dans le RowTag
		      Self.RowTag(Self.ListCount-1) = recordSet.IdxField(3).StringValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		  Self.enabled = true
		  
		  Self.ListIndex = 0
		  For j As Integer = 0 To Self.ListCount-1
		    If Self.RowTag(j) =  table_zonePrimSelect Then
		      Self.ListIndex = j
		      Exit For
		    End If
		  Next
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub loadData(dB As PostgreSQLDatabase, table_name As String, prefix As String , table_zone As String)
		  
		  Self.DeleteAllRows
		  
		  Dim recordSet as RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM " + table_name + " WHERE " + prefix + "_nom = '" + table_zone  +"' " + _
		  "AND " + prefix + "_tri > 0  ORDER BY " + prefix + "_tri"  + " ASC "
		  recordSet =  dB.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      Dim lang as string = Session.Header("Accept-Language")
		      If instr(lang, "fr") > 0 Then
		        Self.AddRow(recordSet.Field( prefix + "_desc_fr").StringValue)
		      Else
		        Self.AddRow(recordSet.Field(prefix + "_desc_en").StringValue)
		      End If
		      
		      // Mettre la valeur de la clé dans le RowTag
		      Self.RowTag(Self.ListCount-1) = recordSet.IdxField(4).StringValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		  Self.enabled = true
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub loadDataPrim(dB As PostgreSQLDatabase, table_name As String, prefix As String, field_select As String, field_sort1 As String, Optional field_order1 As String , Optional field_sort2 As String , Optional field_order2 As String)
		  
		  Self.DeleteAllRows
		  
		  Dim recordSet as RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT " + prefix + "_id, " + field_select + " FROM " + table_name +  " ORDER BY " + field_sort1 
		  
		  If field_order1 <> "" then strSQL = strSQL + "  " + field_order1
		  If field_sort2 <> "" then strSQL = strSQL + ", " + field_sort2
		  If field_order2 <> "" then strSQL = strSQL + " " + field_order2
		  
		  recordSet =  dB.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      Self.AddRow(recordSet.IdxField(2).StringValue)
		      // Mettre la valeur de la clé dans le RowTag
		      Self.RowTag(Self.ListCount-1) = recordSet.Field(prefix + "_id").IntegerValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		    
		  End If
		  
		  recordSet = Nil
		  Self.enabled = true
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub loadDataSec(dB As PostgreSQLDatabase, table_name As String, prefix As String ,  table_zone As String, table_zoneSec As String, table_zoneSelect As String, table_zoneSecSelect As String)
		  
		  Self.DeleteAllRows
		  
		  Dim recordSet as RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM " + table_name + _
		  " WHERE " + prefix + "_nom = '" + table_zone  +"' "  + _
		  "    AND "  + prefix + "_cleetr_nom = '" + table_zoneSec  +"' " + _
		  "    AND "  + prefix + "_tri > 0 " + _
		  "    AND "  + prefix + "_cleetr_cle = ( SELECT " + prefix + "_tri from " + table_name + _
		  " WHERE " + prefix + "_nom = '" + table_zoneSec  +"' " + _
		  " AND " + prefix + "_cle = '" + table_zoneSelect + "' )"+_
		  " ORDER BY " + prefix + "_tri"  + " ASC "
		  recordSet =  dB.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      dim lang as string = Session.Header("Accept-Language")
		      if instr(lang, "fr")>0 then
		        Self.AddRow(recordSet.Field( prefix + "_desc_fr").StringValue)
		      Else
		        Self.AddRow(recordSet.Field(prefix + "_desc_en").StringValue)
		      End If
		      
		      // Mettre la valeur de la clé dans le RowTag
		      Self.RowTag(Self.ListCount-1) = recordSet.IdxField(4).StringValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		  Self.enabled = true
		  
		  Self.ListIndex = 0
		  For j As Integer = 0 To Self.ListCount-1
		    If Self.RowTag(j) =  table_zoneSecSelect Then
		      Self.ListIndex = j
		      Exit For
		    End If
		  Next
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub loadTablePrimaire(dB As PostgreSQLDatabase, table_name As String, prefix As String)
		  
		  Self.DeleteAllRows
		  
		  Dim recordSet as RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM " + table_name + " WHERE " + prefix + "_tri = 0  ORDER BY " + prefix + "_tri  ASC "
		  recordSet =  dB.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    While Not recordSet.EOF
		      Dim lang as string = Session.Header("Accept-Language")
		      If instr(lang, "fr") > 0 Then
		        Self.AddRow(recordSet.Field( prefix + "_desc_fr").StringValue)
		      Else
		        Self.AddRow(recordSet.Field(prefix + "_desc_en").StringValue)
		      End If
		      
		      // Mettre la valeur de la clé dans le RowTag
		      Self.RowTag(Self.ListCount-1) = recordSet.IdxField(2).StringValue
		      recordSet.MoveNext
		    Wend
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		  Self.enabled = true
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SelectValue(value As String) As Integer
		  // Search for the specified value in the list and select it.
		  // If no value is found then Return -1.
		  '
		  For j As Integer = 0 To Self.ListCount-1
		    If Self.RowTag(j) =  value Then
		      Return j
		    End If
		  Next
		  
		  Return -1
		  
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		table_cle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		table_desc_en As String
	#tag EndProperty

	#tag Property, Flags = &h0
		table_desc_fr As String
	#tag EndProperty

	#tag Property, Flags = &h0
		table_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		table_tri As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Cursor"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
			EditorType="Enum"
			#tag EnumValues
				"0 - Automatic"
				"1 - Standard Pointer"
				"2 - Finger Pointer"
				"3 - IBeam"
				"4 - Wait"
				"5 - Help"
				"6 - Arrow All Directions"
				"7 - Arrow North"
				"8 - Arrow South"
				"9 - Arrow East"
				"10 - Arrow West"
				"11 - Arrow Northeast"
				"12 - Arrow Northwest"
				"13 - Arrow Southeast"
				"14 - Arrow Southwest"
				"15 - Splitter East West"
				"16 - Splitter North South"
				"17 - Progress"
				"18 - No Drop"
				"19 - Not Allowed"
				"20 - Vertical IBeam"
				"21 - Crosshair"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Enabled"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Height"
			Visible=true
			Group="Behavior"
			InitialValue="22"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HelpTag"
			Visible=true
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HorizontalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ListIndex"
			Visible=true
			Group="Behavior"
			InitialValue="-1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockBottom"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockHorizontal"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockLeft"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockRight"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockTop"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LockVertical"
			Visible=true
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="table_cle"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="table_desc_en"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="table_desc_fr"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="table_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="table_tri"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="TabOrder"
			Visible=true
			Group="Behavior"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Text"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="VerticalCenter"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Visible"
			Visible=true
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Width"
			Visible=true
			Group="Behavior"
			InitialValue="100"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ZIndex"
			Group="Behavior"
			InitialValue="1"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_DeclareLineRendered"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_HorizontalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_IsEmbedded"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Locked"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_NeedsRendering"
			Group="Behavior"
			InitialValue="True"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OfficialControl"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_OpenEventFired"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_VerticalPercent"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

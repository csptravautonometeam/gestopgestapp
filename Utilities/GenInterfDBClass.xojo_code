#tag Class
Protected Class GenInterfDBClass
	#tag Method, Flags = &h0
		Sub assignControlToProperties(webView As Webview, prefix As String)
		  Dim webObj As WebObject
		  
		  For i As Integer = 0 To webview.ControlCount-1
		    webObj = webview.ControlAtIndex(i)
		    
		    Dim cleControl As String = extract_Field_Name(webObj.Name)
		    
		    // Ne pas vérifier quand le control n'est pas dans la liste des zones de la classe chargée
		    Dim prop As Introspection.PropertyInfo = Self.propertyDict.Lookup(cleControl, nil)
		    If prop is Nil Then Continue
		    
		    Dim prefixNomProp As String = mid(cleControl, 1, len(prefix))
		    If prefixNomProp <> prefix Then Continue
		    
		    Select Case webObj
		      
		    Case IsA WebTextField
		      Dim webTextField As WebTextField = WebTextField(webObj)
		      
		      Select Case prop.PropertyType.FullName
		        
		      Case "Boolean"
		        Dim boolean as String = webTextField.Text
		        If boolean = "True" Then
		          prop.value(Self) = True
		        Else
		          prop.value(Self) = False
		        End If
		      Case "Currency"
		        prop.value(Self) = CType(val(webTextField.Text),Currency)
		      Case "Date"
		        prop.value(Self) = Nil
		        Dim converti as Boolean
		        Dim laDate as New Date
		        converti = ParseDate(webTextField.Text,laDate)
		        if converti then prop.value(Self) =laDate
		      Case "Double"
		        prop.value(Self) = CType(val(webTextField.Text),Double)
		      Case "Integer"
		        prop.value(Self) = CType(val(webTextField.Text), Integer)
		      Case "Int32"
		        prop.value(Self) = CType(val(webTextField.Text), Int32)
		      Case "Int64"
		        prop.value(Self) = CType(val(webTextField.Text), Int64)
		        'Case "Picture"
		        'prop.Value(Self)  = rS.Field(prop.name).PictureValue
		      Case "String"
		        prop.value(Self) = webTextField.Text
		        
		      End Select
		      
		      
		    Case IsA WebTextArea
		      Dim webTextArea As WebTextArea = WebTextArea(webObj)
		      
		      Select Case prop.PropertyType.FullName
		        
		      Case "String"
		        prop.value(Self) = webTextArea.Text
		        
		      End Select
		      
		      
		    Case IsA WebPopupMenu
		      Dim webPopupMenu As WebPopupMenu = WebPopupMenu(webObj)
		      
		      Select Case prop.PropertyType.FullName
		        
		      Case "Integer"
		        prop.value(Self) = webPopupMenu.ListIndex +1
		      Case "Int32"
		        prop.value(Self) = webPopupMenu.ListIndex +1
		      Case "Int64"
		        prop.value(Self) = webPopupMenu.ListIndex +1
		      Case "String"
		        prop.value(Self) = webPopupMenu.RowTag(webPopupMenu.ListIndex)
		        
		      End Select
		      
		      
		    Case IsA WebRadioGroup
		      Dim webRadioGroup As WebRadioGroup = WebRadioGroup(webObj)
		      
		      Select Case prop.PropertyType.FullName
		        
		      Case "String"
		        Dim droit As Integer = CType(webRadioGroup.SelectedCell.Right,Integer)
		        droit = droit + 1
		        prop.value(Self) = str(droit)
		        
		      End Select
		      
		    End Select
		    
		  Next
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub assignPropertiesToControls(webView As Webview, prefix As String)
		  Dim webObj As WebObject
		  
		  For i As Integer = 0 To webview.ControlCount-1
		    webObj = webview.ControlAtIndex(i)
		    Dim cleControl As String = extract_Field_Name(webObj.Name)
		    Dim prefixNomProp As String = mid(cleControl, 1, len(prefix))
		    If prefixNomProp <> prefix Then Continue
		    
		    Select Case webObj
		      
		    Case IsA WebTextField
		      Dim webTextField As WebTextField = WebTextField(webObj)
		      Dim prop As Introspection.PropertyInfo = Self.propertyDict.Lookup(cleControl, nil)
		      
		      If not (prop is nil) then
		        
		        Select Case prop.PropertyType.FullName
		          
		        Case "Boolean"
		          webTextField.Text  = str(prop.value(Self).BooleanValue)
		        Case "Currency"
		          webTextField.Text  = str(prop.value(Self).CurrencyValue)
		        Case "Date"
		          webTextField.Text = mid(str(prop.value(Self).DateValue),1,10)
		        Case "Double"
		          webTextField.Text  = str(prop.value(Self).DoubleValue)
		        Case "Integer"
		          webTextField.Text  = str(prop.value(Self).IntegerValue)
		        Case "Int32"
		          webTextField.Text  = str(prop.value(Self).Int32Value)
		        Case "Int64"
		          webTextField.Text  = str(prop.value(Self).Int64Value)
		          'Case "Picture"
		          'prop.Value(Self)  = rS.Field(prop.name).PictureValue
		        Case "String"
		          webTextField.Text = prop.value(Self).StringValue
		          
		        End Select
		        
		      End If
		      
		    Case IsA WebTextArea
		      Dim webTextArea As WebTextArea = WebTextArea(webObj)
		      Dim prop As Introspection.PropertyInfo = Self.propertyDict.Lookup(cleControl, nil)
		      
		      If not (prop is nil) then
		        
		        Select Case prop.PropertyType.FullName
		          
		        Case "String"
		          webTextArea.Text = prop.value(Self).StringValue
		        End Select
		        
		      End If
		      
		    Case IsA WebPopupMenu
		      Dim webPopupMenu As WebPopupMenu = WebPopupMenu(webObj)
		      Dim prop As Introspection.PropertyInfo = Self.propertyDict.Lookup(cleControl, nil)
		      
		      If not (prop is nil) then
		        
		        Select Case prop.PropertyType.FullName
		          
		        Case "Integer"
		          webPopupMenu.ListIndex  = prop.value(Self).IntegerValue - 1
		        Case "Int32"
		          webPopupMenu.ListIndex  = prop.value(Self).Int32Value - 1
		        Case "Int64"
		          webPopupMenu.ListIndex  = prop.value(Self).Int64Value - 1
		        Case "String"
		          webPopupMenu.ListIndex = 0
		          For j As Integer = 0 To webPopupMenu.ListCount-1
		            Dim webpop1 As String = webPopupMenu.RowTag(j)
		            Dim webpop2 As String = prop.value(Self).StringValue
		            If webPopupMenu.RowTag(j) =  prop.value(Self).StringValue Then
		              webPopupMenu.ListIndex = j
		              Exit For
		            End If
		          Next
		          
		        End Select
		      End If
		      
		    Case IsA WebRadioGroup
		      Dim webRadioGroup As WebRadioGroup = WebRadioGroup(webObj)
		      Dim prop As Introspection.PropertyInfo = Self.propertyDict.Lookup(cleControl, nil)
		      
		      If not (prop is nil) then
		        
		        Select Case prop.PropertyType.FullName
		          
		        Case "String"
		          // Par défaut, assigner droit à 2 qui résultera en une valeur de Non pour le webRadioGroup
		          Dim str As String = prop.value(Self).StringValue
		          Dim droit As Integer = 2
		          If str <> "" Then droit = CType(val(prop.value(Self).StringValue),Integer)
		          droit = droit - 1
		          webRadioGroup.CellSelected(0,droit) =  True
		        End Select
		        
		      End If
		      
		    End Select
		    
		    
		  Next
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function check_If_Locked(dB As PostgreSQLDatabase, recordID As String) As String
		  //Vérifie si l'enregistrement a été verrouillé par un autre utilisateur
		  
		  //Calcul du seuil d'expiration pour la date
		  Using xojo.core
		  Dim expiration As New DateInterval
		  expiration.Seconds = Session.timeOutDuration
		  Dim lockTime As Xojo.Core.Date = Xojo.Core.Date.Now
		  Dim refTime As Xojo.Core.Date = Date.FromText(lockTime.ToText) - expiration 
		  Dim refTimeString As String = refTime.ToText
		  Dim userNom As String = username
		  
		  //Vérification
		  Dim strSQL As String = "SELECT * FROM lock_entries  WHERE  record_id = $1 AND user_id <> $2 AND timestamp >= $3 "
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  
		  pgps.Bind(0, recordID)
		  pgps.Bind(1, username)
		  pgps.Bind(2, refTime.ToText)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If dB.Error Then
		    Return GMTextConversion(dB.ErrorMessage)
		  End If
		  
		  If RS <> Nil And Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    lockedBy = RS.Field("user_id").StringValue
		    recordAvailable = False
		    RS.Close
		    RS = Nil
		    Return "Locked"
		  End If
		  
		  lockedBy = ""
		  recordAvailable = True
		  Return "Unlocked"
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function cleanLocks(dB As PostgreSQLDatabase) As String
		  //Calcul du seuil d'expiration pour les dates à effacer
		  Using xojo.core
		  Dim expiration As New DateInterval
		  expiration.Seconds = Session.timeOutDuration
		  Dim refTime As xojo.Core.Date = xojo.Core.Date.Now - expiration
		  
		  
		  //Effacement des verrous périmés
		  Dim strSQL As String = "DELETE FROM lock_entries WHERE timestamp < $1"
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, refTime.ToText)
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  
		  
		  //Gestion Erreur
		  If dB.Error Then
		    Dim message As String = GMTextConversion(dB.ErrorMessage)
		    dB.Rollback
		    Return message
		  End If
		  
		  
		  dB.Commit
		  lockedRowID = 0
		  Return "Succes"
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function cleanUserLocks(dB As PostgreSQLDatabase) As String
		  //Effacement des verrous de l'utilisateur courant
		  Dim strSQL As String = "DELETE FROM lock_entries WHERE user_id = $1"
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, username)
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  
		  
		  //Gestion Erreur
		  If dB.Error Then
		    Dim message As String = GMTextConversion(dB.ErrorMessage)
		    dB.Rollback
		    Return message
		  End If
		  
		  
		  dB.Commit
		  lockedRowID = 0
		  Return "Succes"
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor(dB As PostgreSQLDatabase, tableName As String, prefix As String)
		  // Dictionnaire des propriétés de la classe
		  Dim propDict As New Dictionary
		  Dim ti as Introspection.typeinfo = Introspection.GetType(Self)
		  
		  For each prop As Introspection.PropertyInfo in ti.GetProperties()
		    If prop.IsPublic and prop.CanRead and prop.CanWrite Then
		      propDict.Value(prop.Name) = prop
		      
		      Select Case prop.PropertyType.FullName
		        
		      Case "Boolean"
		        prop.Value(Self)  = False
		      Case "Currency"
		        prop.Value(Self)  = 0
		      Case "Date"
		        prop.Value(Self)  = new Date
		      Case "Double"
		        prop.Value(Self)  = 0
		      Case "Integer"
		        prop.Value(Self)  = 0
		      Case "Int32"
		        prop.Value(Self)  = 0
		      Case "Int64"
		        prop.Value(Self)  = 0
		        'Case "Picture"
		        'prop.Value(Self)  = rS.Field(prop.name).PictureValue
		      Case "String"
		        prop.Value(Self)  = ""
		        
		      End Select
		      
		    End If
		  Next
		  
		  If Not(Self.propertyDict Is Nil) Then Self.propertyDict = Nil
		  Self.propertyDict = propDict
		  
		  // Dictionnaire servant à la validation 
		  Dim propValidDict As New Dictionary
		  Dim donneesValidationRS As RecordSet
		  donneesValidationRS = lireDonneesValidation(dB, tableName , prefix)
		  
		  If donneesValidationRS <> Nil Then
		    
		    For i As Integer = 1 To donneesValidationRS.RecordCount
		      
		      Dim valeurValidation As String = donneesValidationRS.Field("udt_name").StringValue + "," + _
		      donneesValidationRS.Field("character_maximum_length").StringValue + "," + _
		      donneesValidationRS.Field("numeric_precision").StringValue + "," + _
		      donneesValidationRS.Field("numeric_scale").StringValue
		      propValidDict.Value(donneesValidationRS.Field("column_name").StringValue) = valeurValidation
		      
		      donneesValidationRS.MoveNext
		    Next
		    
		  End If
		  
		  If Not(Self.propertyValidationDict Is Nil) Then Self.propertyValidationDict = Nil
		  Self.propertyValidationDict = propValidDict
		  donneesValidationRS.Close
		  donneesValidationRS = Nil
		  
		  //Identifier l'utilisateur (le username = email)
		  username = getUserInfo
		  //MsgBox (username)
		  //Identifier le profil de sécurité
		  securityProfile = getUserSecurityProfile(username)
		  //MsgBox (securityProfile)
		  //Extraire l'id de l'utilisateur
		  //user_id = get_userId(dB, username)
		  //MsgBox (str(user_id))
		  
		  //deleteCookies
		  Dim usernametrav As String = username
		  Dim sec As String = securityProfile
		  //Fermeture de la session si l'utilisateur ne passe pas la sécurité
		  //If username = "" Or securityProfile = "" Or user_id = 0 Then
		  If username = "" Or securityProfile = "" Then
		    Dim ML6probMsgBox As New ProbMsgModal
		    ML6ProbMsgBox.WLAlertLabel.Text = Self.PROBLEMEDESECURITE
		    ML6probMsgBox.Show
		    Session.quitter
		    Exit Sub
		  End If
		  
		  // Mettre le user dans une variable custom Postgresql
		  Session.bdTechEol.SQLExecute("SET application.currentuser TO '" + username + "';")
		  
		  // Mettre à jour les infos de surveillance de session
		  updateDataSessionHist(dB)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub coupeStringLongueurMaxBD(webView As Webview)
		  Dim webObj As WebObject
		  
		  For i As Integer = 0 To webview.ControlCount-1
		    webObj = webview.ControlAtIndex(i)
		    
		    Dim cleControl As String = extract_Field_Name(webObj.Name)
		    
		    // Ne pas vérifier quand le control n'est pas dans la liste des zones de la classe chargée
		    Dim prop As Introspection.PropertyInfo = Self.propertyDict.Lookup(cleControl, nil)
		    If prop is Nil Then Continue
		    
		    Dim validation As String = ""
		    validation = Self.propertyValidationDict.Lookup(cleControl, nil)
		    If validation = "" Then Continue
		    
		    Dim fieldValidation(-1) As String = Split(validation, ",")
		    Dim fieldType As String = fieldValidation(0)
		    If FieldType <> "varchar"  Then Continue
		    
		    Dim fieldStringLength As Integer = val(fieldValidation(1))
		    
		    
		    Select Case webObj
		      
		    Case IsA WebTextField
		      Dim webTextField As WebTextField = WebTextField(webObj)
		      webTextField.text = Left(webTextField.text,fieldStringLength)
		      
		      
		    Case IsA WebTextArea
		      Dim webTextArea As WebTextArea = WebTextArea(webObj)
		      webTextArea.text = Left(webTextArea.text,fieldStringLength)
		      
		    End Select
		    
		    
		  Next
		  
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub deleteCookies()
		  
		  //Supprimer  les cookies de l'application 
		  Session.Cookies.Remove(Session.COOKIEUSER, ".", "")
		  Session.Cookies.Remove(Session.COOKIEOEMSECURITYPROFILE, ".", "")
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function extract_Field_Name(field As String) As String
		  
		  // Extraire le nom du champ en laissant le suffixe avec le souligné
		  
		  Dim fieldTable(-1) As String = Split(field, "_")
		  
		  Dim indexLastElement As Integer = Ubound(fieldTable)
		  
		  Return Replace(field, "_"+fieldTable(IndexLastElement), "")
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function extract_prefix(field As String) As String
		  
		  // Extraire le préfixe du champ en laissant le suffixe avec le souligné
		  
		  Dim prefixTable(-1) As String = Split(field, ".")
		  
		  Dim indexLastElement As Integer = Ubound(prefixTable)
		  
		  Return prefixTable(IndexLastElement)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function extract_Table_Field_Type(field As String) As String
		  
		  // Extraire le type pour un champ (ex. String, Integer, etc.)
		  
		  Dim prop As Introspection.PropertyInfo = Self.propertyDict.Value(field)
		  Return prop.PropertyType.FullName
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function extract_Table_Nom_bis() As String
		  // Le nom de la classe est toujours du type Globals.nomClass
		  // Extraire le nom de la table consiste à enlever Globals et le suffixe Class dans la première version
		  Dim ti as Introspection.TypeInfo = Introspection.GetType(Self)
		  Dim table_Name As String = ""
		  
		  Dim travArray(-1) as String = split(ti.FullName,".")
		  If travArray.Ubound = 0 Then
		    table_Name = ReplaceAll(travArray(0), "Class", "")
		  Else
		    table_Name = ReplaceAll(travArray(1), "Class", "")
		  End If
		  return table_name
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getNewKey(dB As PostgreSQLDatabase, table_name As String, prefix As String) As Integer
		  
		  Dim rs As RecordSet
		  rs = dB .SQLSelect("SELECT Max(" + prefix + "_id) FROM " + table_Name)
		  
		  If rs <> Nil And Not rs.EOF Then
		    Return rs.IdxField(1).IntegerValue + 1
		  Else
		    Return 1
		  End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getUserInfo() As String
		  //Vérification de la présence du cookie
		  
		  If Session.Cookies.Value(Session.COOKIEUSER) <> "" Then
		    Dim cookieContent As String = Session.Cookies.Value(Session.COOKIEUSER)
		    Return cookieContent
		  Else
		    Return ""
		  End If
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function getUserSecurityProfile(pUserName As String) As String
		  
		  If Session.Cookies.Value(Session.COOKIEOEMSECURITYPROFILE) <> "" Then
		    Dim cookieContent As String = Session.Cookies.Value(Session.COOKIEOEMSECURITYPROFILE)
		    Return cookieContent
		  Else
		    Return ""
		  End If
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function get_userId(dB As PostgreSQLDatabase, pUsername As String) As Integer
		  //Générer l'id de l'utilisateur connecté
		  
		  Dim strSQL As String = "SELECT portail_user_id FROM portail_user" + _
		  " WHERE portail_user_code = $1"
		  
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, pUsername)
		  
		  Dim RS As RecordSet = pgps.SQLSelect
		  
		  If RS <> Nil  And Not (RS.BOF And RS.EOF) And RS.RecordCount > 0 Then
		    Return rs.IdxField(1).IntegerValue
		  Else
		    Return 0
		  End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function insertDataBD(prefix As String, newKey as Integer) As DatabaseRecord
		  Dim row As New DatabaseRecord
		  // Extraire le nom de la table qui correspond au nom de la classe moins le suffixe Class
		  Dim table_Name_Key As String = prefix + "_id"
		  
		  For Each key As String In Self.propertyDict.Keys
		    Dim prop As Introspection.PropertyInfo = Self.propertyDict.Value(key)
		    
		    Dim prefixNomProp As String = mid(prop.Name, 1, len(prefix))
		    If prefixNomProp <> prefix Then Continue
		    
		    Select Case prop.PropertyType.FullName
		      
		    Case "Boolean"
		      row.BooleanColumn(key) = prop.Value(Self).BooleanValue
		    Case "Currency"
		      row.CurrencyColumn(key) =prop.Value(Self).CurrencyValue
		    Case "Date"
		      row.DateColumn(key) =prop.Value(Self).DateValue
		    Case "Double"
		      row.DoubleColumn(key) = prop.Value(Self).DoubleValue
		    Case "Integer"
		      If key = table_Name_Key then
		        row.IntegerColumn(key) = newkey
		      Else
		        row.IntegerColumn(key) = prop.Value(Self).IntegerValue
		      End If
		    Case "Int32"
		      If key = table_Name_Key then
		        row.IntegerColumn(key) = newkey
		      Else
		        row.IntegerColumn(key) = prop.Value(Self).IntegerValue
		      End If
		    Case "Int64"
		      row.Int64Column(key) = prop.Value(Self).Int64Value
		    Case "Picture"
		      row.PictureColumn(key) = prop.Value(Self)
		    Case "String"
		      row.Column(key) =prop.Value(Self).StringValue
		    End Select
		    
		  Next
		  
		  return row
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub lireDonneesBD(rS as RecordSet, prefix As String)
		  
		  For Each key As String In Self.propertyDict.Keys
		    Dim prop As Introspection.PropertyInfo = Self.propertyDict.Value(key)
		    
		    Dim prefixNomProp As String = mid(prop.Name, 1, len(prefix))
		    If prefixNomProp <> prefix Then Continue
		    
		    Select Case prop.PropertyType.FullName
		      
		    Case "Boolean"
		      prop.Value(Self)  = rS.Field(prop.name).BooleanValue
		    Case "Currency"
		      prop.Value(Self)  = rS.Field(prop.name).CurrencyValue
		    Case "Date"
		      prop.Value(Self)  = rS.Field(prop.name).DateValue
		    Case "Double"
		      prop.Value(Self)  = rS.Field(prop.name).DoubleValue
		    Case "Integer"
		      prop.Value(Self)  = rS.Field(prop.name).IntegerValue
		    Case "Int32"
		      prop.Value(Self)  = rS.Field(prop.name).IntegerValue
		    Case "Int64"
		      prop.Value(Self)  = rS.Field(prop.name).Int64Value
		    Case "Picture"
		      prop.Value(Self)  = rS.Field(prop.name).PictureValue
		    Case "String"
		      If rS.Field(prop.name) = Nil Then
		        prop.Value(Self)  = ""
		      Else
		        prop.Value(Self)  = rS.Field(prop.name).StringValue
		      End If
		      
		    End Select
		    
		  Next
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function lireDonneesValidation(dB As PostgreSQLDatabase, table_name As String, prefix As String) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT column_name, udt_name, character_maximum_length, numeric_precision, numeric_scale FROM information_schema.columns " + _
		  "WHERE table_name = '" + table_Name + "' AND substring(column_name, 1, " + str(len(prefix)) + ") = '" + prefix + "' Order By column_name" 
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub listDataByField(dB As PostgreSQLDatabase, table_name As String, prefix As String,  field As String, value As String, orderBy1 as String, Optional orderBy2 as String, Optional orderBy3 as String, Optional orderBy4 as String)
		  
		  Dim recordSet As RecordSet
		  recordSet = Self.loadDataByField(dB, table_name, field, value, orderBy1, orderBy2, orderBy3, orderBy4)
		  
		  If recordSet <> Nil Then
		    Self.LireDonneesBD(recordSet, prefix)
		  End If
		  
		  recordSet.Close
		  recordSet = Nil
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub loadControlDict(webView As Webview)
		  Dim controlDict As new Dictionary
		  Dim webObj As WebObject
		  
		  For i As Integer = 0 To webview.ControlCount-1
		    webObj = webview.ControlAtIndex(i)
		    
		    Select Case webObj
		      
		    Case IsA WebTextField
		      controlDict.Value(webObj.Name) = WebTextField(webobj)
		      
		    Case IsA WebPopupMenu
		      controlDict.Value(webObj.Name) = WebPopupMenu(webobj)
		      
		    End Select
		    
		  Next
		  
		  If Not(Self.controlDict Is Nil) Then Self.controlDict = Nil
		  Self.controlDict = controlDict
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadData(dB As PostgreSQLDatabase, table_name As String, orderBy1 as String, Optional orderBy2 as String, Optional orderBy3 as String, Optional orderBy4 as String) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  
		  strSQL = "SELECT * FROM " + table_Name + " Order By  " + orderBy1
		  
		  If orderBy2 <> "" then strSQL = strSQL + " ," + orderBy2
		  If orderBy3 <> "" then strSQL = strSQL + " ," + orderBy3
		  If orderBy4 <> "" then strSQL = strSQL + " ," + orderBy4
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataByField(dB As PostgreSQLDatabase, table_name As String, field As String, value As String, orderBy1 as String, Optional orderBy2 as String, Optional orderBy3 as String, Optional orderBy4 as String) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String = ""
		  Dim compteur As Integer
		  
		  // Extraire le type du champ field
		  Dim fieldType As String = extract_Table_Field_Type(field)
		  
		  Select Case fieldType
		    
		  Case "Integer"
		    strSQL = "SELECT * FROM " +  table_Name + " where  " + field  + " = " + value + " Order By  " + orderBy1
		  Case "Int32"
		    strSQL = "SELECT * FROM " + table_Name + " where  " + field  + " = " + value + " Order By  " + orderBy1
		  Case "Int64"
		    strSQL = "SELECT * FROM " + table_Name + " where  " + field  + " = " + value + " Order By  " + orderBy1
		  Case "String"
		    strSQL = "SELECT * FROM " + table_Name + " where  " + field  + " = '" + value + "' Order By  " + orderBy1
		    
		  End Select
		  
		  
		  If orderBy2 <> "" then strSQL = strSQL + " ," + orderBy2
		  If orderBy3 <> "" then strSQL = strSQL + " ," + orderBy3
		  If orderBy4 <> "" then strSQL = strSQL + " ," + orderBy4
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataByFieldCond(dB As PostgreSQLDatabase , table_name As String, field1 As String, operateur1 As String, value1 As String, connecteur As String, field2 As String, operateur2 As String, value2 As String, orderBy1 as String, Optional orderBy2 as String, Optional orderBy3 as String, Optional orderBy4 as String) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String = ""
		  Dim clauseWhere As  String = ""
		  Dim compteur As Integer
		  
		  // Extraire le type du champ field1
		  Dim fieldType As String = extract_Table_Field_Type(field1)
		  
		  Select Case fieldType
		    
		  Case "Integer"
		    clauseWhere = " where  " + field1  + operateur1 + value1
		  Case "Int32"
		    clauseWhere = " where  " + field1  + operateur1 + value1
		  Case "Int64"
		    clauseWhere = " where  " + field1  + operateur1 + value1
		  Case "String"
		    clauseWhere = " where  " + field1  + operateur1 + "'" + value1 + "' "
		    
		  End Select
		  
		  If field2 <> "" Then
		    // Extraire le type du champ field2
		    fieldType = extract_Table_Field_Type(field2)
		    
		    Select Case fieldType
		      
		    Case "Integer"
		      clauseWhere = clauseWhere + connecteur + field2  + operateur2 + value2
		    Case "Int32"
		      clauseWhere = clauseWhere + connecteur + field2  + operateur2 + value2
		    Case "Int64"
		      clauseWhere = clauseWhere + connecteur + field2  + operateur2 + value2
		    Case "String"
		      clauseWhere = clauseWhere + connecteur + field2  + operateur2 + "'" + value2 + "' "
		      
		    End Select
		    
		  End If
		  
		  
		  
		  strSQL = "SELECT * FROM " + table_Name + clauseWhere + " Order By  " + orderBy1
		  
		  
		  If orderBy2 <> "" then strSQL = strSQL + " ," + orderBy2
		  If orderBy3 <> "" then strSQL = strSQL + " ," + orderBy3
		  If orderBy4 <> "" then strSQL = strSQL + " ," + orderBy4
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function loadDataByFieldSort(dB As PostgreSQLDatabase, table_name As String, field As String, value As String, orderBy1 as String, Optional sort1 as String, Optional orderBy2 as String, Optional sort2 as String) As RecordSet
		  Dim genRS As RecordSet
		  Dim strSQL As  String = ""
		  Dim compteur As Integer
		  
		  // Extraire le type du champ field
		  Dim fieldType As String = extract_Table_Field_Type(field)
		  
		  Select Case fieldType
		    
		  Case "Integer"
		    strSQL = "SELECT * FROM " +  table_Name + " where  " + field  + " = " + value + " Order By  " + orderBy1
		  Case "Int32"
		    strSQL = "SELECT * FROM " + table_Name + " where  " + field  + " = " + value + " Order By  " + orderBy1
		  Case "Int64"
		    strSQL = "SELECT * FROM " + table_Name + " where  " + field  + " = " + value + " Order By  " + orderBy1
		  Case "String"
		    strSQL = "SELECT * FROM " + table_Name + " where  " + field  + " = '" + value + "' Order By  " + orderBy1
		    
		  End Select
		  
		  
		  If sort1 <> "" then strSQL = strSQL + " " + sort1
		  If orderBy2 <> "" then strSQL = strSQL + " ," + orderBy2
		  If sort2 <> "" then strSQL = strSQL + " " + sort2
		  
		  genRS =  dB.SQLSelect(strSQL)
		  compteur = genRS.RecordCount
		  
		  return genRS
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function lockRow(dB As PostgreSQLDatabase) As String
		  Dim lockedRowIDBis As Integer = 0
		  Dim lockTime As Xojo.Core.Date = Xojo.Core.Date.Now
		  Dim strSQL As String = "INSERT INTO lock_entries VALUES(DEFAULT, $1, $2, $3) RETURNING lock_id"
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, recordID)
		  pgps.Bind(1, username)
		  pgps.Bind(2, lockTime.ToText)
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  //Saisir l'ID du verrou créé
		  Dim RS As Recordset =pgps.SQLSelect
		  
		  lockedRowID = 0
		  //Gestion Erreur
		  If dB.Error Then
		    Dim message As String = GMTextConversion(dB.ErrorMessage)
		    dB.Rollback
		    Return message
		  End If
		  
		  If RS <> Nil And Not (RS.BOF And RS.EOF) Then
		    lockedRowID = RS.Field("lock_id").IntegerValue
		  End If
		  
		  lockedRowIDBis = lockedRowID
		  Session.sauvegardeLockedRowID = lockedRowID
		  
		  dB.Commit
		  recordID = ""
		  
		  Return "Succes"
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function supprimerDataByField(dB As PostgreSQLDatabase, table_name As String,  field As String, value As String) As String
		  // Les données  sont contenues dans la variable de Session
		  
		  Dim messageErreur as String = ""
		  
		  Dim rS As RecordSet
		  rS = Self.loadDataByField(dB, table_name, field, value, field)
		  
		  if rS <> Nil Then
		    // Démarrer une transaction pour pouvoir la renverser s'il y a un problème
		    dB.SQLExecute("BEGIN TRANSACTION")
		    
		    rS.DeleteRecord
		    rS.Close
		    rS = Nil
		    
		    If  dB.Error Then
		      messageErreur = dB.ErrorMessage
		      dB.rollback
		      Return messageErreur
		      
		    Else
		      dB.commit
		      Return "Succes"
		    End If
		  End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function unLockRow(dB As PostgreSQLDatabase) As String
		  Dim lockRowIndex As Integer = Session.sauvegardeLockedRowID
		  Dim strSQL As String = "DELETE FROM lock_entries WHERE lock_id = $1"
		  Dim pgps As PostgreSQLPreparedStatement = dB.Prepare(strSQL)
		  pgps.Bind(0, Session.sauvegardeLockedRowID)
		  
		  dB.SQLExecute("BEGIN TRANSACTION")
		  pgps.SQLExecute
		  
		  //Gestion Erreur
		  If dB.Error Then
		    Dim message As String = GMTextConversion(dB.ErrorMessage)
		    dB.Rollback
		    Return message
		  End If
		  
		  dB.Commit
		  lockedRowID = 0
		  Return "Succes"
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub updateDataSessionHist(dB As PostgreSQLDatabase)
		  
		  Dim sessionhist_user As String = username
		  
		  Dim sessionhist_web_browser As String =  ""
		  Select Case Session.Browser
		  Case WebSession.BrowserType.Android
		    sessionhist_web_browser = "Android"
		  Case WebSession.BrowserType.Blackberry
		    sessionhist_web_browser = "Blackberry"
		  Case WebSession.BrowserType.Chrome
		    sessionhist_web_browser = "Chrome"
		  Case WebSession.BrowserType.ChromeOS
		    sessionhist_web_browser = "ChromeOS"
		  Case WebSession.BrowserType.Firefox
		    sessionhist_web_browser = "Firefox"
		  Case WebSession.BrowserType.InternetExplorer
		    sessionhist_web_browser = "Internet Explorer"
		  Case WebSession.BrowserType.Opera
		    sessionhist_web_browser = "Opera"
		  Case WebSession.BrowserType.OperaMini
		    sessionhist_web_browser = "Opera Mini"
		  Case WebSession.BrowserType.Safari
		    sessionhist_web_browser = "Safari"
		  Case WebSession.BrowserType.SafariMobile
		    sessionhist_web_browser = "Safari Mobile"
		  Case WebSession.BrowserType.Epiphany
		    sessionhist_web_browser = "Epiphany on Raspberry Pi"
		  Case WebSession.BrowserType.Unknown
		    sessionhist_web_browser = "Unknown web browser"
		  End Select
		  
		  Dim sessionhist_identifier As String = Session.Identifier
		  Dim sessionhist_lang_code  As String = Session.LanguageCode
		  
		  Dim sessionhist_platform  As String = ""
		  Select Case Session.Platform
		  Case WebSession.PlatformType.AndroidPhone
		    sessionhist_platform = "AndroidPhone"
		  Case WebSession.PlatformType.AndroidTablet
		    sessionhist_platform = "AndroidTablet"
		  Case WebSession.PlatformType.Blackberry
		    sessionhist_platform = "Blackberry"
		  Case WebSession.PlatformType.iPad
		    sessionhist_platform = "iPad"
		  Case WebSession.PlatformType.iPhone
		    sessionhist_platform = "iPhone"
		  Case WebSession.PlatformType.iPodTouch
		    sessionhist_platform = "iPodTouch"
		  Case WebSession.PlatformType.Linux
		    sessionhist_platform = "Linux"
		  Case WebSession.PlatformType.Macintosh
		    sessionhist_platform = "Macintosh"
		  Case WebSession.PlatformType.PS3
		    sessionhist_platform = "PS3"
		  Case WebSession.PlatformType.WebOS
		    sessionhist_platform = "WebOS"
		  Case WebSession.PlatformType.Wii
		    sessionhist_platform = "Wii"
		  Case WebSession.PlatformType.Windows
		    sessionhist_platform = "Windows"
		  Case WebSession.PlatformType.Unknown
		    sessionhist_platform = "Unknown platform type"
		  End Select
		  
		  Dim sessionhist_remote_address As String = Session.RemoteAddress
		  Dim sessionhist_status_message  As String = Session.StatusMessage
		  Dim sessionhist_title As String =  Session.Title  
		  Dim sessionhist_url  As String = Session.URL
		  
		  // Nom de la classe appelante
		  Dim thisClassTypeInfo As Introspection.TypeInfo = Introspection.GetType(Self)
		  Dim sessionhist_classname As String = thisClassTypeInfo.Name
		  
		  Dim strSQL As String = "INSERT INTO sessionhist VALUES(nextval('sessionhist_id_seq')"+ _
		  ", '" + sessionhist_user + "'"+ _
		  ", '" + sessionhist_classname + "'"+ _
		  ", '" + sessionhist_web_browser + "'"+ _
		  ", '" + sessionhist_identifier + "'"+ _
		  ", '" + sessionhist_lang_code + "'"+ _
		  ", '" + sessionhist_platform + "'"+ _
		  ", '" + sessionhist_remote_address + "'"+ _
		  ", '" + sessionhist_status_message + "'"+ _
		  ", '" + sessionhist_title + "'"+ _
		  ", '" + sessionhist_url + "')"
		  
		  db.SQLExecute("DELETE FROM sessionhist WHERE sessionhist_identifier = '" + sessionhist_identifier +"'" )
		  
		  dB.SQLExecute(strSQL)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function valideZoneNumerique(webView As Webview) As String
		  Dim webObj As WebObject
		  Dim messageErreur as String = "Succes"
		  
		  For i As Integer = 0 To webview.ControlCount-1
		    webObj = webview.ControlAtIndex(i)
		    If Not (webObj isA WebTextField) Then Continue
		    
		    Dim cleControl As String = extract_Field_Name(webObj.Name)
		    
		    // Ne pas vérifier quand le control n'est pas dans la liste des zones de la classe chargée
		    Dim prop As Introspection.PropertyInfo = Self.propertyDict.Lookup(cleControl, nil)
		    If prop is Nil Then Continue
		    
		    // Ne pas valider s'il n'y a pas de correspondance dans la BD
		    Dim validation As String = ""
		    validation = Self.propertyValidationDict.Lookup(cleControl, nil)
		    If validation = "" Then Continue
		    
		    // Ne pas valider si vide
		    Dim webTextField As WebTextField = WebTextField(webObj)
		    If  webTextField.text = "" Then Continue
		    
		    Dim fieldValidation(-1) As String = Split(validation, ",")
		    Dim fieldType As String = fieldValidation(0)
		    If FieldType = "varchar" Or  FieldType = "date" Then Continue
		    
		    Dim fieldLongeurAvantPoint As Integer = val(fieldValidation(2)) - val(fieldValidation(3))
		    Dim fieldLongeurApresPoint As Integer = val(fieldValidation(3))
		    
		    Dim nombreATraiter As String = ""
		    nombreATraiter = webTextField.text
		    
		    // Remplacer les virgules par un point
		    nombreATraiter = Replace(nombreATraiter,",",".")
		    
		    // Vérifier si on a vraiment affaire à un nombre
		    If Not IsNumeric(nombreATraiter) Then
		      webTextField.Style = TextFieldErrorStyle
		      messageErreur = ZONEDOITETRENUMERIQUE
		    End If
		    
		    // Enlever les chiffres de trop qui dépasse le maximum
		    Dim chiffreArray(-1) As String = Split(nombreATraiter, ".")
		    Dim chiffreAvantLePoint As String = chiffreArray(0)
		    If len(chiffreAvantLePoint) > fieldLongeurAvantPoint Then
		      If fieldLongeurApresPoint = 0 Then // Nombre entier, Si le nombre de chiffres dépasse la limite -> Tronquer
		        If len(chiffreAvantLePoint) > fieldLongeurAvantPoint Then nombreATraiter = Right(chiffreAvantLePoint,fieldLongeurAvantPoint) 
		      Else
		        If len(chiffreAvantLePoint) > fieldLongeurAvantPoint Then nombreATraiter = Right(chiffreAvantLePoint,fieldLongeurAvantPoint) + "." + chiffreArray(1)
		      End If
		    End If
		    
		    webTextField.Text = nombreATraiter
		    
		  Next
		  
		  Return messageErreur
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function writeData(dB As PostgreSQLDatabase, table_name As String, prefix As String) As String
		  
		  Dim valeurNouvelleCle as Integer = 0
		  // Lire le contenu de la variable correspondante a ID
		  Dim table_Name_Key As String = prefix + "_id"
		  Dim prop As Introspection.PropertyInfo = Self.propertyDict.Lookup(table_Name_Key, nil)
		  If not (prop is nil) Then
		    Dim propType As String = prop.PropertyType.FullName
		    valeurNouvelleCle = prop.value(Self).IntegerValue
		  End If
		  
		  Dim messageErreur as String = db.ErrorMessage
		  
		  // C'est un nouvel enregistrement?
		  If valeurNouvelleCle = 0 Then
		    valeurNouvelleCle = Self.getNewKey(dB, table_name, prefix)
		    dB.InsertRecord(table_Name, Self.insertDataBD(prefix, valeurNouvelleCle))
		    If  dB .Error Then
		      messageErreur = GMTextConversion(dB.ErrorMessage)
		      Return messageErreur
		    Else
		      // Donner la nouvelle valeur à la propriété Clé
		      prop.value(Self) = valeurNouvelleCle
		      Return "Succes"
		    End If
		    
		    Exit
		  End If
		  
		  Dim rS As RecordSet
		  rS = Self.loadDataByField(dB, table_name, table_Name_Key, str(valeurNouvelleCle), table_Name_Key)
		  
		  if rS <> Nil Then
		    // Démarrer une transaction pour pouvoir la renverser s'il y a un problème
		    dB .SQLExecute("BEGIN TRANSACTION")
		    rS.Edit
		    Self.writeDataBD(rS, prefix)
		    rS.Update
		    rS.Close
		    rS = Nil
		    
		    If  dB.Error Then
		      messageErreur = dB .ErrorMessage
		      dB.Rollback
		      Return messageErreur
		      
		    Else
		      dB.Commit
		      Return "Succes"
		    End If
		  End If
		  
		  
		  
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub writeDataBD(rS as RecordSet, prefix As String)
		  
		  For Each key As String In Self.propertyDict.Keys
		    Dim prop As Introspection.PropertyInfo = Self.propertyDict.Value(key)
		    
		    Dim prefixNomProp As String = mid(prop.Name, 1, len(prefix))
		    If prefixNomProp <> prefix Then Continue
		    
		    Dim propName As String = prop.Name
		    Select Case prop.PropertyType.FullName
		      
		    Case "Boolean"
		      rS.Field(key).BooleanValue = prop.Value(Self).BooleanValue
		    Case "Currency"
		      rS.Field(key).CurrencyValue =prop.Value(Self).CurrencyValue
		    Case "Date"
		      rS.Field(key).DateValue = prop.Value(Self).DateValue
		    Case "Double"
		      rS.Field(key).DoubleValue = prop.Value(Self).DoubleValue
		    Case "Integer"
		      rS.Field(key).IntegerValue = prop.Value(Self).IntegerValue
		    Case "Int32"
		      rS.Field(key).IntegerValue = prop.Value(Self).IntegerValue
		    Case "Int64"
		      rS.Field(key).Int64Value = prop.Value(Self).Int64Value
		    Case "Picture"
		      rS.Field(key).PictureValue = prop.Value(Self)
		    Case "String"
		      If rS.Field(key) = Nil Then
		        prop.Value(Self)  = ""
		      Else
		        rS.Field(key).StringValue =prop.Value(Self).StringValue
		      End If
		    End Select
		    
		  Next
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		controlDict As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		lockedBy As String
	#tag EndProperty

	#tag Property, Flags = &h0
		lockEditMode As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		lockedRowID As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		propertyDict As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		propertyValidationDict As Dictionary
	#tag EndProperty

	#tag Property, Flags = &h0
		recordAvailable As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		recordID As String
	#tag EndProperty

	#tag Property, Flags = &h0
		securityProfile As String
	#tag EndProperty

	#tag Property, Flags = &h0
		username As String
	#tag EndProperty

	#tag Property, Flags = &h0
		uuuser_id As Integer
	#tag EndProperty


	#tag Constant, Name = ERRLOCK, Type = String, Dynamic = True, Default = \"An error occured while trying to lock records. Please inform your database administrator.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"An error occured while trying to lock records. Please inform your database administrator."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Une erreur s\'est produite lors du verrouillage de l\'enregistrement. S.V.P. contactez l\'administrateur de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"An error occured while trying to lock records. Please inform your database administrator."
	#tag EndConstant

	#tag Constant, Name = PROBLEMEDESECURITE, Type = String, Dynamic = True, Default = \"Probl\xC3\xA8me de s\xC3\xA9curit\xC3\xA9", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Probl\xC3\xA8me de s\xC3\xA9curit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Probl\xC3\xA8me de s\xC3\xA9curit\xC3\xA9"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Security problem"
	#tag EndConstant

	#tag Constant, Name = ZONEDOITETRENUMERIQUE, Type = String, Dynamic = True, Default = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette zone doit \xC3\xAAtre num\xC3\xA9rique"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This field must be a numeric value"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

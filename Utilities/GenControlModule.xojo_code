#tag Module
Protected Module GenControlModule
	#tag Method, Flags = &h0
		Function extract_Field_Name(field As String) As String
		  
		  // Extraire le nom du champ en laissant le suffixe avec le souligné
		  
		  Dim fieldTable(-1) As String = Split(field, "_")
		  
		  Dim indexLastElement As Integer = Ubound(fieldTable)
		  
		  Return Replace(field, "_"+fieldTable(IndexLastElement), "")
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h1
		Protected Sub resetControls(webView As Webview, initContent As Boolean, initStyle As Boolean, bloquerZone As Boolean, Optional propertyDict as Dictionary)
		  Dim webObj As WebObject
		  
		  For i As Integer = 0 To webview.ControlCount-1
		    webObj = webview.ControlAtIndex(i)
		    
		    If propertyDict <> Nil Then
		      Dim cleObj As String = webObj.Name
		      Dim cleControl As String = extract_Field_Name(webObj.Name)
		      Dim prop As Introspection.PropertyInfo = propertyDict.Lookup(cleControl, nil)
		      If prop Is Nil Then Continue
		    End If
		    
		    Select Case webObj
		      
		    Case IsA WebButton
		      Dim webButton As WebButton
		      webButton = WebButton(webObj)
		      webButton.Enabled = True
		      If bloquerZone = True Then webButton.Enabled = False
		      
		    Case IsA WebHTMLViewer
		      Dim webHTMLViewer As WebHTMLViewer
		      webHTMLViewer = WebHTMLViewer(webObj)
		      If initContent Then webHTMLViewer.URL = ""
		      
		    Case IsA WebImageView
		      Dim webImageView As WebImageView
		      webImageView = WebImageView(webObj)
		      webImageView.Enabled = True
		      If bloquerZone = True Then webImageView.Enabled = False
		      
		    Case IsA WebPopupMenu
		      Dim webPopupMenu As WebPopupMenu
		      webPopupMenu = WebPopupMenu(webObj)
		      If initContent Then 
		        webPopupMenu.DeleteAllRows
		      End If
		      If initStyle Then webPopupMenu.Style = Nil
		      webPopupMenu.Enabled = True
		      If bloquerZone = True Then webPopupMenu.Enabled = False
		      
		    Case IsA WebRadioGroup
		      Dim webRadioGroup As WebRadioGroup
		      webRadioGroup = WebRadioGroup(webObj)
		      If initContent Then 
		        webRadioGroup.SelectedCell = New Pair(0,0)
		      End If
		      If initStyle Then 
		        webRadioGroup.Style = Nil
		      End If
		      webRadioGroup.Enabled = True
		      If bloquerZone = True Then 
		        webRadioGroup.Enabled = False
		      End If
		      
		    Case IsA WebSegmentedControl
		      Dim webSegmentedControl As WebSegmentedControl
		      webSegmentedControl = WebSegmentedControl(webObj)
		      webSegmentedControl.Enabled = True
		      If bloquerZone = True Then webSegmentedControl.Enabled = False
		      
		    Case IsA WebTextArea
		      Dim webTextArea As WebTextArea
		      webTextArea = WebTextArea(webObj)
		      If initContent Then webTextArea.Text = ""
		      If initStyle Then webTextArea.Style = Nil
		      webTextArea.Enabled = True
		      If bloquerZone = True Then webTextArea.Enabled = False
		      
		    Case IsA WebTextField
		      Dim webTextField As WebTextField
		      webTextField = WebTextField(webObj)
		      If initContent Then webTextField.Text = ""
		      If initStyle Then webTextField.Style = Nil
		      webTextField.Enabled = True
		      If bloquerZone = True Then webTextField.Enabled = False
		      
		    End Select
		  Next
		  
		End Sub
	#tag EndMethod


	#tag Constant, Name = CARRIAGERETURN, Type = String, Dynamic = True, Default = \"\r", Scope = Public
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMEPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"techeol"
	#tag EndConstant

	#tag Constant, Name = CENTRALDATABASENAMETEST, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"techeol"
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"ml6cartier01.cartierenergie.com"
	#tag EndConstant

	#tag Constant, Name = HOSTPHOTOTEST, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"192.168.0.51"
	#tag EndConstant

	#tag Constant, Name = HOSTPREPRODUCTION, Type = String, Dynamic = True, Default = \"www.ml6techeol.tech", Scope = Public
	#tag EndConstant

	#tag Constant, Name = HOSTPROD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"localhost"
	#tag EndConstant

	#tag Constant, Name = HOSTTEST, Type = String, Dynamic = True, Default = \"192.168.0.51", Scope = Public
	#tag EndConstant

	#tag Constant, Name = LEAVEAPPMESSAGE, Type = String, Dynamic = True, Default = \"Vous \xC3\xAAtes en train de quitter cette application.\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You are about to leave this application."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous \xC3\xAAtes en train de quitter cette application."
	#tag EndConstant

	#tag Constant, Name = PASSWORD, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"postgres"
	#tag EndConstant

	#tag Constant, Name = PHOTOFOLDER, Type = String, Dynamic = True, Default = \"baseplan/reppale", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"baseplan/reppale"
	#tag EndConstant

	#tag Constant, Name = USER, Type = String, Dynamic = True, Default = \"", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"admin"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

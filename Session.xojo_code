#tag Class
Protected Class Session
Inherits WebSession
	#tag Event
		Sub Close()
		  
		  If Self.bdTechEol <> Nil Then
		    
		    // Enlever les infos associés à la session
		    Self.bdTechEol.SQLExecute("DELETE FROM sessionhist WHERE sessionhist_identifier = '" + Session.identifier +"'" )
		    Self.bdTechEol.Close
		    Self.bdTechEol = Nil
		  End If
		  
		  Session.Quit
		End Sub
	#tag EndEvent

	#tag Event
		Sub Open()
		  //Initialize and verify if on Production or Test
		  //We'll use an INI settings file to read the environment value
		  
		  UserSettings = GetFolderItem("ProdOuTest.ini")
		  
		  #if DebugBuild Then
		    UserSettings = GetFolderItem( "").Parent.Child("ProdOuTest.ini")
		  #Endif
		  
		  OpenINI(UserSettings)
		  
		  environnementProp = INI_File.Get("ProductionOuTest","Env","")
		  
		  Session.host = HOSTPROD
		  If Session.environnementProp = "Preproduction" Then  // Nous sommes en préproduction
		    Session.host = HOSTPREPRODUCTION
		  ElseIf Session.environnementProp = "Test" Then  // Nous sommes en test
		    Session.host = HOSTTEST
		  End If
		  
		  //Durée d'inactivité allouée en 10 minutes en secondes
		  Self.Timeout = timeOutDuration
		End Sub
	#tag EndEvent

	#tag Event
		Sub PrepareSession(ByRef HTMLHeader as String)
		  Select Case Me.Platform
		  Case WebSession.PlatformType.iPhone
		    // Allow iPhone 5 to scale web app to use entire screen height when
		    // it is added to the home screen
		    // The "apple-mobile-web-app-status-bar-style" ensures that the
		    // status bar remains visible
		    HTMLHeader = "<meta name=""viewport"" content=""initial-scale=1.0"" />" + _
		    EndOfLine + "<meta name=""apple-mobile-web-app-status-bar-style"" content=""black"" />"
		    
		  Case WebSession.PlatformType.iPad
		    // Force the viewport to be exactly 1012 wide when the user connects with an iPad
		    HTMLHeader = "<meta name=""viewport"" content=""width=1012"">"
		    
		  End Select
		End Sub
	#tag EndEvent

	#tag Event
		Sub TimedOut()
		  
		  //Session.bdTechEol.appDBConnected = False
		  environnementProp = ""
		  INI_File = Nil
		  //If sSecurite <>Nil Then
		  //sSecurite = Nil
		  //End If
		  
		  Session.ShowURL("http://www.ml6.tech/page_deconnexion/index.html", False)
		  
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h0
		Sub OpenINI(strFile as FolderItem)
		  INI_File=new INISettings(strFile)
		  INI_File.Load
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub quitter()
		  //Supprimer  les cookies de l'application 
		  Session.Cookies.Remove(Session.COOKIEUSER, ".", "")
		  Session.Cookies.Remove(Session.COOKIEOEMSECURITYPROFILE, ".", "")
		  
		  //Terminer la session
		  If Session.environnementProp = "Preproduction" Then  // Nous sommes en test
		    ShowURL("http://techeolpreprod.ml6.tech:7070", False)
		  ElseIf Session.environnementProp = "Test" Then  // Nous sommes en test
		    ShowURL("http://" + GenControlModule.HOSTTEST + ":7070", False)
		  Else
		    ShowURL("http://www.ml6techeol.tech:7070", False)
		  End If
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		bdTechEol As PostgreSQLDatabase
	#tag EndProperty

	#tag Property, Flags = &h0
		centralDatabaseName As String
	#tag EndProperty

	#tag Property, Flags = &h0
		connected As Boolean = False
	#tag EndProperty

	#tag Property, Flags = &h0
		environnementProp As String
	#tag EndProperty

	#tag Property, Flags = &h0
		host As String
	#tag EndProperty

	#tag Property, Flags = &h0
		INI_File As INISettings
	#tag EndProperty

	#tag Property, Flags = &h0
		mobileUser As Boolean
	#tag EndProperty

	#tag Property, Flags = &h0
		pointeurParametreWebDialog As ParametreWebDialog
	#tag EndProperty

	#tag Property, Flags = &h0
		sauvegardeLockedRowID As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		sUser As UserClass
	#tag EndProperty

	#tag Property, Flags = &h0
		timeOutDuration As Integer = 600
	#tag EndProperty

	#tag Property, Flags = &h0
		UserSettings As FolderItem
	#tag EndProperty


	#tag Constant, Name = COOKIEOEMSECURITYPROFILE, Type = String, Dynamic = True, Default = \"User_Security_Profile", Scope = Public
	#tag EndConstant

	#tag Constant, Name = COOKIEUSER, Type = String, Dynamic = True, Default = \"Portail_UserName", Scope = Public
	#tag EndConstant

	#tag Constant, Name = ERRORCONNECTION, Type = String, Dynamic = True, Default = \"Connection to the database has failed", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Connection to the database has failed"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"La connexion \xC3\xA0 la base de donn\xC3\xA9es a \xC3\xA9chou\xC3\xA9e\r\n"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Connection to the database has failed\r"
	#tag EndConstant

	#tag Constant, Name = ErrorDialogCancel, Type = String, Dynamic = True, Default = \"Do Not Send", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Do Not Send"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ne pas envoyer"
	#tag EndConstant

	#tag Constant, Name = ErrorDialogMessage, Type = String, Dynamic = True, Default = \"This application has encountered an error and cannot continue.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This application has encountered an error and cannot continue."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette application doit s\'arr\xC3\xAAter."
	#tag EndConstant

	#tag Constant, Name = ErrorDialogQuestion, Type = String, Dynamic = True, Default = \"Please describe what you were doing right before the error occurred:", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Please describe what you were doing right before the error occurred:"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"S.V.P. d\xC3\xA9crivez ce que vous tentiez d\'accomplir juste avant que l\'erreur se produise :"
	#tag EndConstant

	#tag Constant, Name = ErrorDialogSubmit, Type = String, Dynamic = True, Default = \"Send", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Send"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Envoyer"
	#tag EndConstant

	#tag Constant, Name = ErrorThankYou, Type = String, Dynamic = True, Default = \"Thank You", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Thank You"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Merci!"
	#tag EndConstant

	#tag Constant, Name = ErrorThankYouMessage, Type = String, Dynamic = True, Default = \"Your feedback helps us make improvements.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Your feedback helps us make improvements.\r"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vos commentaires contribuent \xC3\xA0 vous offrir la meilleure exp\xC3\xA9rience possible.\r\n"
	#tag EndConstant

	#tag Constant, Name = NoJavascriptInstructions, Type = String, Dynamic = True, Default = \"To turn Javascript on\x2C please refer to your browser settings window.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"To turn Javascript on\x2C please refer to your browser settings window."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Pour activer Java\x2C r\xC3\xA9f\xC3\xA9rez-vous \xC3\xA0 l\'aide de votre fureteur.\r"
	#tag EndConstant

	#tag Constant, Name = NoJavascriptMessage, Type = String, Dynamic = True, Default = \"Javascript must be enabled to access this page.", Scope = Public
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Javascript must be enabled to access this page.\r"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Javascript doit \xC3\xAAtre activ\xC3\xA9 pour pouvoir acc\xC3\xA9der \xC3\xA0 cette application."
	#tag EndConstant

	#tag Constant, Name = SuccessConnection, Type = String, Dynamic = True, Default = \"You are connected to the database", Scope = Public
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous \xC3\xAAtes connect\xC3\xA9 \xC3\xA0 la base de donn\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You are connected to the database"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="ActiveConnectionCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Browser"
			Group="Behavior"
			Type="BrowserType"
			EditorType="Enum"
			#tag EnumValues
				"0 - Unknown"
				"1 - Safari"
				"2 - Chrome"
				"3 - Firefox"
				"4 - InternetExplorer"
				"5 - Opera"
				"6 - ChromeOS"
				"7 - SafariMobile"
				"8 - Android"
				"9 - Blackberry"
				"10 - OperaMini"
				"11 - Epiphany"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="BrowserVersion"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="centralDatabaseName"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ConfirmMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="connected"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Connection"
			Group="Behavior"
			Type="ConnectionType"
			EditorType="Enum"
			#tag EnumValues
				"0 - AJAX"
				"1 - WebSocket"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="environnementProp"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="GMTOffset"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HashTag"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="HeaderCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="host"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Identifier"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LanguageCode"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="LanguageRightToLeft"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="mobileUser"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="PageCount"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Platform"
			Group="Behavior"
			Type="PlatformType"
			EditorType="Enum"
			#tag EnumValues
				"0 - Unknown"
				"1 - Macintosh"
				"2 - Windows"
				"3 - Linux"
				"4 - Wii"
				"5 - PS3"
				"6 - iPhone"
				"7 - iPodTouch"
				"8 - Blackberry"
				"9 - WebOS"
				"10 - iPad"
				"11 - AndroidTablet"
				"12 - AndroidPhone"
				"13 - RaspberryPi"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="Protocol"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RemoteAddress"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="RenderingEngine"
			Group="Behavior"
			Type="EngineType"
			EditorType="Enum"
			#tag EnumValues
				"0 - Unknown"
				"1 - WebKit"
				"2 - Gecko"
				"3 - Trident"
				"4 - Presto"
				"5 - EdgeHTML"
			#tag EndEnumValues
		#tag EndViewProperty
		#tag ViewProperty
			Name="sauvegardeLockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="ScaleFactor"
			Group="Behavior"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="StatusMessage"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Timeout"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="timeOutDuration"
			Group="Behavior"
			InitialValue="600"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Title"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="URL"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_baseurl"
			Group="Behavior"
			Type="string"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_Expiration"
			Group="Behavior"
			InitialValue="-1"
			Type="Double"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_hasQuit"
			Group="Behavior"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="_mConnection"
			Group="Behavior"
			Type="ConnectionType"
			EditorType="Enum"
			#tag EnumValues
				"0 - AJAX"
				"1 - WebSocket"
			#tag EndEnumValues
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

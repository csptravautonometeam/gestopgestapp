#tag Module
Protected Module Globals
	#tag Method, Flags = &h0
		Sub activateWebDialog(mytext As String, boxtype As String)
		  Dim refWC As WebDialog
		  
		  Select Case boxtype
		  Case is = "I"
		    If Session.CurrentPage.ControlWithName("InfoMsgModalDialog") <> Nil Then
		      refWC =  WebDialog(Session.CurrentPage.ControlWithName("InfoMsgModalDialog"))
		      Globals.dialogText = mytext
		      refWC.Show
		    End If
		  Case is = "P"
		    If Session.CurrentPage.ControlWithName("ProbMsgModalDialog") <> Nil Then
		      refWC =  WebDialog(Session.CurrentPage.ControlWithName("ProbMsgModalDialog"))
		      Globals.dialogText = mytext
		      refWC.Show
		    End If
		  Case is = "Q"
		    If Session.CurrentPage.ControlWithName("QMsgModalDialog") <> Nil Then
		      refWC =  WebDialog(Session.CurrentPage.ControlWithName("QMsgModalDialog"))
		      Globals.dialogText = mytext
		      refWC.Show
		    End If
		  Case is = "D"
		    If Session.CurrentPage.ControlWithName("DEMsgModalDialog") <> Nil Then
		      refWC =  WebDialog(Session.CurrentPage.ControlWithName("DEMsgModalDialog"))
		      Globals.dialogText = mytext
		      refWC.Show
		    End If
		  End select
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function GMTextConversion(tempString As String) As String
		  Dim c as TextConverter
		  c=GetTextConverter(GetTextEncoding(&h500), GetTextEncoding(0))
		  return c.convert( tempString)
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function SafeSQL(TempString As String) As String
		  Dim goodString As String
		  goodString = ReplaceAll(TempString, "'", "''")
		  Return goodString
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub selectListItem(targetList As WebListBox, itemID As Integer, itemWithID As Boolean)
		  Dim selectFirst As Boolean = True
		  
		  If itemWithID Then
		    For i As Integer = 0 To targetList.RowCount -1
		      If targetList.RowTag(i).IntegerValue = itemID Then
		        targetList.Selected(i) = True
		        selectFirst = False
		        Exit For i
		      End If
		    Next i
		    
		    If selectFirst Then targetList.Selected(0) = True
		    
		  Else
		    If targetList.RowCount > 0 Then
		      If itemID <= targetList.RowCount -1 And itemID >-1 Then
		        targetList.Selected(itemID) = True
		      Else
		        targetList.Selected(0) = True
		      End If
		    Else
		      Exit Sub
		    End If
		  End If
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		dialogText As String
	#tag EndProperty

	#tag Property, Flags = &h0
		modalArray() As WebContainer
	#tag EndProperty

	#tag Property, Flags = &h0
		showInactifs As Boolean = False
	#tag EndProperty


	#tag Constant, Name = ERRDATABASE, Type = String, Dynamic = True, Default = \"Database Error", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Database Error"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Database Error"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Erreur de base de donn\xC3\xA9es"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="dialogText"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="showInactifs"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

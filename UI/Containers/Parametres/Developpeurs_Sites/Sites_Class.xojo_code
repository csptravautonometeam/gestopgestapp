#tag Class
Protected Class Sites_Class
	#tag Method, Flags = &h0
		Sub Constructor()
		  //Récupérer la liste des applications de l'utilisateur et générer le dictionnaire
		  
		  get_dev_sites_list(Dev_Class.dev_id) 
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub get_dev_sites_list(devID As Integer)
		  Dim strSQL As String = "SELECT * FROM dbglobal.param_site WHERE dbglobal.param_site_developpeur_id = $1"
		  
		  Dim pgps As PostgreSQLPreparedStatement =Session.bdTechEol.Prepare(strSQL)
		  pgps.Bind(0, CStr(devID))
		  
		  Session.bdTechEol.SQLExecute("BEGIN TRANSACTION")
		  sites_list = pgps.SQLSelect
		  
		  If Not Session.bdTechEol.Error Then
		    Session.bdTechEol.Commit
		  Else
		    Session.bdTechEol.Rollback
		    Dim ML6probMsgBox As New ProbMsgModal
		    Globals.dialogText = ERRDATABASE
		    ML6ProbMsgBox.WLAlertLabel.Text = Globals.dialogText
		    ML6probMsgBox.Show
		  End If
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function siteExists(site_id As Integer) As Boolean
		  get_dev_sites_list(Dev_Class.dev_id)
		  
		  If sites_list <> Nil And Not (sites_list.BOF And sites_list.EOF) Then
		    While Not sites_list.EOF
		      //Dim x as string = sites_list.Field("param_site_id").StringValue
		      If sites_list.Field("param_site_id").IntegerValue = site_id Then
		        Return True
		      End If
		      sites_list.MoveNext
		    Wend
		  End If
		  
		  Return False
		End Function
	#tag EndMethod


	#tag Property, Flags = &h0
		Shared sites_list As RecordSet
	#tag EndProperty

	#tag Property, Flags = &h0
		sites_listbox_id As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="sites_listbox_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

#tag Module
Protected Module DeveloppeursSitesModule
	#tag Constant, Name = AFFICHERINACTIFS, Type = String, Dynamic = True, Default = \"Show inactives", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Show inactives"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Afficher inactifs"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Show inactives"
	#tag EndConstant

	#tag Constant, Name = AJOUTER, Type = String, Dynamic = True, Default = \"Add", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Add"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ajouter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Add"
	#tag EndConstant

	#tag Constant, Name = ANNULER, Type = String, Dynamic = True, Default = \"Annuler", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Cancel"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Annuler"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Cancel"
	#tag EndConstant

	#tag Constant, Name = CODE, Type = String, Dynamic = True, Default = \"Code", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Code"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Code"
	#tag EndConstant

	#tag Constant, Name = DBSCHEMANAME, Type = String, Dynamic = True, Default = \"dbglobal", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DBTABLENAME, Type = String, Dynamic = True, Default = \"param_site", Scope = Protected
	#tag EndConstant

	#tag Constant, Name = DESCRIPTION, Type = String, Dynamic = True, Default = \"Description", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Description"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Description"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Description"
	#tag EndConstant

	#tag Constant, Name = DEVELOPER, Type = String, Dynamic = True, Default = \"Developer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Developer"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Exploitant"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Developer"
	#tag EndConstant

	#tag Constant, Name = DEVELOPERNAME, Type = String, Dynamic = True, Default = \"Developer Name", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Developer Name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom exploitant"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Developer Name"
	#tag EndConstant

	#tag Constant, Name = EMPLACEMENT, Type = String, Dynamic = True, Default = \"Location", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Location"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Emplacement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Location"
	#tag EndConstant

	#tag Constant, Name = ERRCREATEENTRY, Type = String, Dynamic = True, Default = \"The new enty couldn\'t be created.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The new enty couldn\'t be created.\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"La nouvelle entr\xC3\xA9e n\'a pu \xC3\xAAtre cr\xC3\xA9\xC3\xA9e."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The new enty couldn\'t be created."
	#tag EndConstant

	#tag Constant, Name = ERRDATAENTRY, Type = String, Dynamic = True, Default = \"The information typed is not in the right format. Check if numbers should be used.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The information typed is not in the right format. Check if numbers should be used."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vos informations ne sont pas entr\xC3\xA9es dans le bon format. V\xC3\xA9rifiez si des donn\xC3\xA9es num\xC3\xA9riques sont demand\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The information typed is not in the right format. Check if numbers should be used."
	#tag EndConstant

	#tag Constant, Name = ERRDELETEITEM, Type = String, Dynamic = True, Default = \"The entry couldn\'t be deleted.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The entry couldn\'t be deleted.\r"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"La s\xC3\xA9lection n\'a pu \xC3\xAAtre supprim\xC3\xA9e."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The entry couldn\'t be deleted.\r"
	#tag EndConstant

	#tag Constant, Name = ERRENTRYEXISTS, Type = String, Dynamic = True, Default = \"This enty already exists.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"This enty already exists.\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Cette entr\xC3\xA9e existe d\xC3\xA9j\xC3\xA0."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"This enty already exists."
	#tag EndConstant

	#tag Constant, Name = ERRLOCK, Type = String, Dynamic = True, Default = \"An error occured while trying to lock records. Please inform your database administrator.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"An error occured while trying to lock records. Please inform your database administrator."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Une erreur s\'est produite lors du verrouillage de l\'enregistrement. S.V.P. contactez l\'administrateur de la base de donn\xC3\xA9es."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"An error occured while trying to lock records. Please inform your database administrator."
	#tag EndConstant

	#tag Constant, Name = ERRREQUIREDDATA, Type = String, Dynamic = True, Default = \"These informations are required.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"These informations are required."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Ces informations sont obligatoires."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"These informations are required."
	#tag EndConstant

	#tag Constant, Name = ERRUPDATEITEM, Type = String, Dynamic = True, Default = \"The entry couldn\'t be updated.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"The entry couldn\'t be updated.\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Les informations de la s\xC3\xA9lection n\'ont pu \xC3\xAAtre mises \xC3\xA0 jour."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"The entry couldn\'t be updated."
	#tag EndConstant

	#tag Constant, Name = ERRUSERSTATUS, Type = String, Dynamic = True, Default = \"You must select a status for the user.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"You must select a status for the user.\r"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Vous devez choisir le statut de l\'utilisateur."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"You must select a status for the user.\r"
	#tag EndConstant

	#tag Constant, Name = FABRICANT, Type = String, Dynamic = True, Default = \"Manufacturer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Manufacturer"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Fabricant"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Manufacturer"
	#tag EndConstant

	#tag Constant, Name = FIRSTNAME, Type = String, Dynamic = True, Default = \"First Name", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"First Name"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"First Name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Pr\xC3\xA9nom"
	#tag EndConstant

	#tag Constant, Name = LASTNAME, Type = String, Dynamic = True, Default = \"Last Name", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Last Name"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Last Name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom"
	#tag EndConstant

	#tag Constant, Name = MODIFY, Type = String, Dynamic = True, Default = \"Modify", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Modify"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Modifier"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Modify"
	#tag EndConstant

	#tag Constant, Name = NOM, Type = String, Dynamic = True, Default = \"Name", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Name"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nom"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Name"
	#tag EndConstant

	#tag Constant, Name = NOUVSITE, Type = String, Dynamic = True, Default = \"New Site", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"New Site"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nouveau site"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"New Site"
	#tag EndConstant

	#tag Constant, Name = NUMBER, Type = String, Dynamic = True, Default = \"Number", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"WTG Number"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Nombre Tours"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"WTG Number"
	#tag EndConstant

	#tag Constant, Name = OKENTRYDELETED, Type = String, Dynamic = True, Default = \"Entry deleted.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Entry deleted."
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Entry deleted."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"La s\xC3\xA9lection a \xC3\xA9t\xC3\xA9 supprim\xC3\xA9e."
	#tag EndConstant

	#tag Constant, Name = SERIAL, Type = String, Dynamic = True, Default = \"Serial No.", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Serial No."
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"No. S\xC3\xA9rie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Serial No."
	#tag EndConstant

	#tag Constant, Name = SITE, Type = String, Dynamic = True, Default = \"Site", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Site"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Site"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Site"
	#tag EndConstant

	#tag Constant, Name = SITES, Type = String, Dynamic = True, Default = \"Sites", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sites"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sites"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Sites"
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Status", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Status"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = SUPPRIMER, Type = String, Dynamic = True, Default = \"Supprimer", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Supprimer"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Delete"
	#tag EndConstant

	#tag Constant, Name = TEAMM, Type = String, Dynamic = True, Default = \"Team Manager", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Team Manager"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Chef d\'\xC3\xA9quipe"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Team Manager"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

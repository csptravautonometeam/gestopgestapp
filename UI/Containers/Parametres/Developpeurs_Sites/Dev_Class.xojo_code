#tag Class
Protected Class Dev_Class
	#tag Method, Flags = &h0
		Sub Constructor()
		  
		  get_dev_list
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function devExists(newDevName As String, devID As Integer) As Boolean
		  get_dev_list
		  
		  If dev_list <> Nil And Not (dev_list.BOF And dev_list.EOF) Then
		    While Not dev_list.EOF
		      //Dim x as string = dev_list.Field("param_developpeur_nom").StringValue
		      If dev_list.Field("param_developpeur_nom").StringValue = newDevName And dev_list.Field("param_developpeur_id").IntegerValue <> devID Then
		        Return True
		      End If
		      dev_list.MoveNext
		    Wend
		  End If
		  
		  Return False
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub get_dev_list()
		  //Dim RS_Temp As RecordSet
		  //
		  //If  showInactifs Then
		  //RS_Temp = Session.bdTechEol.loadData(chr(34) + "dbglobal.param_developpeur", "param_developpeur_nom")
		  //Else
		  //RS_Temp = Session.bdTechEol.loadDataByField(chr(34) + "dbglobal.param_developpeur", "param_developpeur_status", "1", "param_developpeur_nom")
		  //End If
		  //
		  //dev_list = RS_Temp
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Shared dev_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		dev_list As RecordSet
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

#tag WebPage
Begin WebContainer ParametreSideBar
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   500
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "931806350"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   500
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   500
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1804472319"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   500
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebListBox ParametreListBox
      AlternateRowColor=   &cEDF3FE00
      ColumnCount     =   5
      ColumnWidths    =   "6%,20%,35%,35%,4%"
      Cursor          =   0
      Enabled         =   True
      HasHeading      =   True
      HeaderStyle     =   "0"
      Height          =   445
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   "#ParametreModule.TRI	#ParametreModule.CLE	#ParametreModule.DESCRIPTIONFRANCAISE	#ParametreModule.DESCRIPTIONANGLAISE	ST"
      Left            =   0
      ListIndex       =   -1
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      MinimumRowHeight=   22
      Multiline       =   True
      PrimaryRowColor =   &cFFFFFF00
      Scope           =   0
      SelectionStyle  =   "0"
      Style           =   "0"
      TabOrder        =   -1
      Top             =   55
      VerticalCenter  =   0
      Visible         =   True
      Width           =   500
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ParametreLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   8
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   ""
      TextAlign       =   0
      Top             =   8
      VerticalCenter  =   0
      Visible         =   True
      Width           =   210
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVAddParametre
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#ParametreModule.HINTADD"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   430
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1075273727
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVDelParametre
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#ParametreModule.HINTSUPPRIMER"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   464
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   1303803903
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVEnrParametre
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   False
      Height          =   30
      HelpTag         =   "#ParametreModule.HINTUPDATEDETAILS"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   380
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   966273023
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TitreTableLabel
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   False
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1056458751"
      TabOrder        =   1
      Text            =   "Titre"
      TextAlign       =   0
      Top             =   21
      VerticalCenter  =   0
      Visible         =   True
      Width           =   237
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVModParametre
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   30
      HelpTag         =   "#ParametreModule.HINTLOCKUNLOCK"
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   346
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   173774847
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   8
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub afficherMessage(message As String)
		  Dim ML6probMsgBox As New ProbMsgModal
		  ML6ProbMsgBox.WLAlertLabel.Text = message
		  ML6probMsgBox.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub ajouterParametre()
		  Dim compteur As Integer
		  compteur = ParametreListBox.RowCount  'Pour Debug
		  compteur = ParametreListBox.LastIndex    'Pour Debug
		  compteur = ParametreListBox.ListIndex ' Pour Debug
		  
		  ParametreListBox.insertRow(compteur+1, str(Self.sParametre.parametre_tri), ParametreModule.NOUVELLECLE, ParametreModule.NOUVELLEDESCRIPTIONFRANCAISE, ParametreModule.NOUVELLEDESCRIPTIONANGLAISE, "A")
		  
		  // Faire pointer le curseur sur le nouveau projet
		  ParametreListBox.ListIndex = ParametreListBox.ListIndex + 1
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne = ParametreListBox.ListIndex
		  
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.tri = Self.sParametre.parametre_tri
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.cle = ParametreModule.NOUVELLECLE
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.desc_fr = ParametreModule.NOUVELLEDESCRIPTIONFRANCAISE
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.desc_en = ParametreModule.NOUVELLEDESCRIPTIONANGLAISE
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.statut = "A"
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode = "Creation"
		  
		  // Création d'un nouvel enregistrement de paramètre
		  // parametre_tri va prendre la valeur de la ligne précédente
		  Self.sParametre.parametre_nom = Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom
		  Self.sParametre.parametre_cle = ParametreModule.NOUVELLECLE
		  Self.sParametre.parametre_desc_fr = ParametreModule.NOUVELLEDESCRIPTIONFRANCAISE
		  Self.sParametre.parametre_desc_en = ParametreModule.NOUVELLEDESCRIPTIONANGLAISE
		  Self.sParametre.parametre_statut = "A"
		  // parametre_cleetr_nom et parametre_cleetr_cle vont prendre la valeur de la ligne précédente
		  
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id = 0  // indique que c'est un nouvel enregistrement
		  Self.sParametre.parametre_id = 0
		  Session.pointeurParametreWebDialog.ParametreContainerDialog.majParametreDetail
		  
		  // Renumérotation de la listbox et de la table paramètre
		  renumerotation
		  
		  // Réafficher tout et se repositionner à la ligne ajoutée
		  populateParametre
		  
		  // Se mettre en mode lock
		  //implementLock
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub changementParametre(idParametre_param As Integer)
		  // Si on est en verrouillage et qu'il y a changement d'enregistrement, on remet les icones pertinents actifs et on bloque l'écran Container 
		  If Self.sParametre.lockEditMode = True Then
		    Self.sParametre.lockEditMode = False
		    // Enlever les verrouillages existants
		    Dim message As String = Self.sParametre.cleanUserLocks(Session.bdTechEol)
		    If  message <> "Succes" Then afficherMessage(message)
		    
		    // Rendre disponible l'ajout et la suppression
		    IVModParametre.Picture = ModModal30x30
		    IVAddParametre.Visible = True
		    IVAddParametre.Enabled = True
		    IVDelParametre.Visible = True
		    IVDelParametre.Enabled = True
		    IVEnrParametre.Visible = False
		    IVEnrParametre.Enabled = False
		  End If
		  
		  // Si la valeur de idParametre  est 0, c'est un nouvel enregistrement, donc pas de lecture de la BD
		  If idParametre_param  = 0 Then
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode = "Creation"
		  Else
		    Self.sParametre.listDataByField(Session.bdTechEol, ParametreModule.PARAMETRETABLEName,ParametreModule.PARAMETREPREFIX,  _
		    "parametre_id", str(Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id), "parametre_tri")
		    Session.pointeurParametreWebDialog.ParametreContainerDialog.listParametreDetail
		  End If
		  
		  //pointeurParametreWebDialog.ParametreContainerDialog.listParametreDetail
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub implementLock()
		  Dim ptrParametreCont As ParametreContainer = Session.pointeurParametreWebDialog.ParametreContainerDialog
		  Dim message As String = ""
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String = ParametreModule.DBSCHEMANAME + "." + ParametreModule.DBTABLENAME + "." + str(Self.sParametre.parametre_id)
		  Message = Self.sParametre.check_If_Locked(Session.bdTechEol, param_recherche)
		  If  Message <> "Locked" And Message <> "Unlocked" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If Message = "Locked" Then
		    ptrParametreCont.IVLockedParametre.Visible = True
		    ptrParametreCont.TLLockedByParametre.Text = Self.sParametre.lockedBy
		    ptrParametreCont.TLLockedByParametre.Visible = True
		    Exit Sub
		  End If
		  
		  //Déverrouiller l'enregistrement si on est actuellement en Verrouillage
		  If Self.sParametre.lockEditMode = True Then
		    message = Self.sParametre.unLockRow(Session.bdTechEol)
		    If  message <> "Succes" Then
		      afficherMessage(message)
		      Exit Sub
		    End If
		    IVModParametre.Picture = ModModal30x30
		    // Rendre disponible l'ajout et la suppression
		    IVAddParametre.Visible = True
		    IVAddParametre.Enabled = True
		    IVDelParametre.Visible = True
		    IVDelParametre.Enabled = True
		    // Libérer 
		    ptrParametreCont.IVLockedParametre.Visible = False
		    ptrParametreCont.TLLockedByParametre.Visible = False
		    IVEnrParametre.Visible = False
		    IVEnrParametre.Enabled = False
		    // Bloquer les zones du container
		    Self.sParametre.lockEditMode = False
		    Session.pointeurParametreWebDialog.ParametreContainerDialog.listParametreDetail
		    Exit Sub
		  End If
		  
		  //Activer le mode Verrouillage
		  Self.sParametre.lockEditMode = True
		  
		  //Creer les valeurs de verrouillage record ID
		  Self.sParametre.recordID = ParametreModule.DBSCHEMANAME + "." +ParametreModule.DBTABLENAME+ "." + str(Self.sParametre.parametre_id)
		  message = Self.sParametre.lockRow(Session.bdTechEol) 
		  If message <> "Succes" Then
		    afficherMessage(message)
		    Exit Sub
		  End If
		  
		  ptrParametreCont.IVLockedParametre.Visible = False
		  ptrParametreCont.TLLockedByParametre.Visible = False
		  IVEnrParametre.Enabled = Self.sParametre.recordAvailable
		  
		  //Activer les fonctions pertinentes
		  IVModParametre.Picture = ModModalOrange30x30
		  IVAddParametre.Visible = False
		  IVAddParametre.Enabled = False
		  IVDelParametre.Visible = False
		  IVDelParametre.Enabled = False
		  IVEnrParametre.Visible = True
		  IVEnrParametre.Enabled = True
		  
		  // Débloquer les zones du container
		  Session.pointeurParametreWebDialog.ParametreContainerDialog.listParametreDetail
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub modParametreDetail()
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub populateParametre()
		  
		  If Self.sParametre = Nil Then
		    Self.sParametre = new ParametreClass()
		  End If
		  
		  // Afficher le titre de la table
		  TitreTableLabel.Text = Self.sParametre.chargementTitreTable(Session.bdTechEol, ParametreModule.PARAMETRETABLENAME, ParametreModule.PARAMETREPREFIX,_
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom)
		  
		  Dim parametreRS As RecordSet
		  parametreRS = Self.sParametre.loadDataByFieldCond(Session.bdTechEol, ParametreModule.PARAMETRETABLEName, _
		  "parametre_nom", " = ", Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom, " And ","parametre_tri", " > ", "0", "parametre_tri")
		  
		  If parametreRS <> Nil Then
		    ParametreListBox.DeleteAllRows
		    
		    For i As Integer = 1 To parametreRS.RecordCount
		      ParametreListBox.AddRow(parametreRS.Field("parametre_tri").StringValue, parametreRS.Field("parametre_cle").StringValue, _
		      parametreRS.Field("parametre_desc_fr").StringValue, parametreRS.Field("parametre_desc_en").StringValue, parametreRS.Field("parametre_statut").StringValue)
		      
		      If parametreRS.Field("parametre_statut").StringValue <> "A" Then
		        For j As Integer = 0 To 4
		          ParametreListBox.CellStyle(i-1, j) = BGGray200
		        Next
		      End If
		      
		      // Garder le numéro de ligne
		      ParametreListBox.RowTag(ParametreListBox.LastIndex) = i -1
		      // Garder l'id numéro du parametre
		      ParametreListBox.CellTag(ParametreListBox.LastIndex,1) = parametreRS.Field("parametre_id").IntegerValue
		      
		      parametreRS.MoveNext
		    Next
		    
		    parametreRS.Close
		    parametreRS = Nil
		    
		    // Positionnement à la ligne gardée en mémoire
		    If ParametreListBox.ListIndex  <> Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne And ParametreListBox.RowCount > 0 Then
		      Dim v As Variant = ParametreListBox.LastIndex
		      v = Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne
		      If Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne > ParametreListBox.LastIndex  Then Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne = ParametreListBox.LastIndex
		      ParametreListBox.ListIndex  = Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne
		      Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id = ParametreListBox.CellTag(Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne,1)
		    End If
		  End If
		  
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub renumerotation()
		  Dim parametreRS As RecordSet
		  Dim compteur As Integer = 0
		  
		  parametreRS = Self.sParametre.loadDataByFieldCond(Session.bdTechEol,  ParametreModule.PARAMETRETABLEName, _
		  "parametre_nom", " = ", Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom, " And ","parametre_tri", " >= ", "0", "parametre_tri", "parametre_id")
		  
		  While Not parametreRS.EOF
		    
		    Self.sParametre.LireDonneesBD(parametreRS, ParametreModule.PARAMETREPREFIX)
		    Self.sParametre.parametre_tri = compteur
		    Dim messageErreur  As String = Self.sParametre.writeData(Session.bdTechEol, ParametreModule.PARAMETRETABLEName, ParametreModule.PARAMETREPREFIX)
		    If (messageErreur <> "Succes") Then
		      Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		      pointeurProbMsgModal.WLAlertLabel.Text = ParametreModule.ERREUR + " " + messageErreur
		      pointeurProbMsgModal.Show
		      Exit
		    End If
		    
		    compteur = compteur+1
		    parametreRS.MoveNext
		  Wend
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub supParametreDetail()
		  Dim ptrParametreCont As ParametreContainer = Session.pointeurParametreWebDialog.ParametreContainerDialog
		  
		  // Vérifier si un enregistrement peut être supprimé
		  Dim messageErreur as String = Self.sParametre.validerSuppression(Session.bdTechEol, Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom, Self.sParametre.parametre_cle)
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = ParametreModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit Sub
		  End If
		  
		  // Vérifier si l'enregistrement est verrouillé par un autre utilisateur
		  Dim param_recherche As String = ParametreModule.DBSCHEMANAME + "." +ParametreModule.DBTABLENAME+ "." + str(Self.sParametre.parametre_id)
		  messageErreur = Self.sParametre.check_If_Locked(Session.bdTechEol, param_recherche)
		  If  messageErreur <> "Locked" And messageErreur <> "Unlocked" Then
		    afficherMessage(messageErreur)
		    Exit Sub
		  End If
		  // Si bloqué, afficher le verrouillage
		  If messageErreur = "Locked" Then
		    ptrParametreCont.IVLockedParametre.Visible = True
		    ptrParametreCont.TLLockedByParametre.Text = Self.sParametre.lockedBy
		    ptrParametreCont.TLLockedByParametre.Visible = True
		    Exit Sub
		  End If
		  
		  // L'id  est contenu dans la variable Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id
		  messageErreur = Self.sParametre.supprimerDataByField(Session.bdTechEol, ParametreModule.PARAMETRETABLEName, "parametre_id",str(Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id))
		  
		  If messageErreur <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = ParametreModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		  Else
		    // Renuméroter et Réafficher la liste des projets
		    Self.renumerotation
		    Self.populateParametre
		  End If
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		parametreStruct As ParametreStructure
	#tag EndProperty

	#tag Property, Flags = &h0
		sParametre As ParametreClass
	#tag EndProperty


	#tag Structure, Name = parametreStructure, Flags = &h0
		cle As String*30
		  desc_en As String*100
		  desc_fr As String*100
		  id As Integer
		  no_ligne As Integer
		  tri as Integer
		  edit_mode As String*30
		  table_nom As String*30
		statut As String*1
	#tag EndStructure


#tag EndWindowCode

#tag Events ParametreListBox
	#tag Event
		Sub SelectionChanged()
		  If Me.ListIndex < 0 Then
		    // Rien n'a été sélectionné, réinitialiser les champs
		    //ClearProjetsFields
		    //EnableFields(False)
		    
		  Else
		    // Un enregistrement a été sélectionné, lire et afficher les données
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne = Me.RowTag(Me.ListIndex)
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id = Me.CellTag(Me.ListIndex,1)
		    changementParametre(Me.CellTag(Me.ListIndex,1))
		  End If
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVAddParametre
	#tag Event
		Sub MouseExit()
		  Self.IVAddParametre.Picture = AjouterModal30x30
		  Self.IVDelParametre.Picture = DeleteModal30x30
		  Self.IVEnrParametre.Picture = Enregistrer30x30
		  Self.IVModParametre.Picture = ModModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddParametre.Picture = AjouterModalTE30x30
		  Self.IVDelParametre.Picture = DeleteModal30x30
		  Self.IVEnrParametre.Picture = Enregistrer30x30
		  Self.IVModParametre.Picture = ModModal30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    ajouterParametre
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVDelParametre
	#tag Event
		Sub MouseExit()
		  Self.IVAddParametre.Picture = AjouterModal30x30
		  Self.IVDelParametre.Picture = DeleteModal30x30
		  Self.IVEnrParametre.Picture = Enregistrer30x30
		  Self.IVModParametre.Picture = ModModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddParametre.Picture = AjouterModal30x30
		  Self.IVDelParametre.Picture = DeleteModalTE30x30
		  Self.IVEnrParametre.Picture = Enregistrer30x30
		  Self.IVModParametre.Picture = ModModal30x30
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  If Me.Enabled Then
		    supParametreDetail
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVEnrParametre
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		  Session.pointeurParametreWebDialog.ParametreContainerDialog.traiteMAJ
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Self.IVAddParametre.Picture = AjouterModal30x30
		  Self.IVDelParametre.Picture = DeleteModal30x30
		  Self.IVEnrParametre.Picture = Enregistrer30x30
		  Self.IVModParametre.Picture = ModModal30x30
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddParametre.Picture = AjouterModal30x30
		  Self.IVDelParametre.Picture = DeleteModal30x30
		  Self.IVEnrParametre.Picture = EnregistrerTE30x30
		  Self.IVModParametre.Picture = ModModal30x30
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events IVModParametre
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		  //Déclenchement du mode édition et déverrouillage ou verrouillage
		  If ParametreListBox.ListIndex <> -1 And ParametreListBox.RowCount > 0 Then
		    implementLock
		  End If
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseExit()
		  Self.IVAddParametre.Picture = AjouterModal30x30
		  Self.IVDelParametre.Picture = DeleteModal30x30
		  Self.IVEnrParametre.Picture = Enregistrer30x30
		  If Self.sParametre.lockEditMode = False Then
		    Self.IVModParametre.Picture = ModModal30x30
		  Else
		    Self.IVModParametre.Picture = ModModalOrange30x30
		  End If
		  
		  
		End Sub
	#tag EndEvent
	#tag Event
		Sub MouseEnter()
		  Self.IVAddParametre.Picture = AjouterModal30x30
		  Self.IVDelParametre.Picture = DeleteModal30x30
		  Self.IVEnrParametre.Picture = Enregistrer30x30
		  Self.IVModParametre.Picture = ModModalTE30x30
		  
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior

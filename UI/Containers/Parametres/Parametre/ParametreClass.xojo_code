#tag Class
Protected Class ParametreClass
Inherits GenInterfDBClass
	#tag Method, Flags = &h0
		Function chargementTitreTable(parametreDB As PostgreSQLDatabase, table_name As String, prefix As String , table_zone As String) As String
		  Dim recordSet as RecordSet
		  Dim strSQL As  String
		  Dim compteur As Integer
		  Dim valeurTitre As String = ""
		  
		  strSQL = "SELECT * FROM " + table_name + " WHERE " + prefix + "_nom = '" + table_zone  +"' " + _
		  "AND " + prefix + "_tri = 0  "
		  recordSet =  parametreDB.SQLSelect(strSQL)
		  compteur = recordSet.RecordCount
		  
		  If recordSet <> Nil And Not recordSet.EOF Then
		    Dim lang as string = Session.Header("Accept-Language")
		    If instr(lang, "fr")>0 Then
		      valeurTitre = recordSet.Field( prefix + "_desc_fr").StringValue
		    Else
		      valeurTitre = recordSet.Field(prefix + "_desc_en").StringValue
		    End If
		    recordSet.Close
		  End If
		  
		  recordSet = Nil
		  
		  Return valeurTitre
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub Constructor()
		  // Calling the overridden superclass constructor.
		  Super.Constructor(Session.bdTechEol, PARAMETRETABLEName, parametreprefix)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerSuppression(parametreDB As PostgreSQLDatabase, table_nom As String, value As String) As String
		  // Vérifier si l'enregistrement sélectionné peut être supprimé
		  Dim recordSet as RecordSet
		  Dim strSQL As String = ""
		  
		  Select Case table_nom
		    
		  Case "reppale_descanomalie"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		  Case "reppale_etat"
		    strSQL = "SELECT * FROM reppale WHERE reppale_rep_etat = '" + value + "'"
		  Case "reppale_gravite"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_gravite = '" + value + "'"
		  Case "reppale_intervention"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_intervention = '" + value + "'"
		  Case "reppale_acces"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_methode_acces = '" + value + "'"
		  Case "reppale_pospremiere"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_premiere = '" + value + "'"
		  Case "reppale_posseconde"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_position_seconde = '" + value + "'"
		  Case "reppale_recouvrement"
		    strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '" + value + "'"
		    If value = "NS" Then strSQL = "SELECT * FROM reppale WHERE reppale_recouvrement = '' Or  reppale_recouvrement = '" + value + "'"
		  Case "responsable"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_responsable = '" + value + "'"
		  Case "sitecode"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_site = '" + value + "'"
		  Case "reppale_statut"
		    strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_statut = '" + value + "'"
		  Case "reppale_typeanomalie"
		    // strSQL = "SELECT * FROM reppaleprojet WHERE reppaleprojet_desc_anomalie = '" + value + "'"
		    
		  End Select
		  
		  recordSet =  parametreDB .SQLSelect(strSQL)
		  Dim compteur As Integer = recordSet.RecordCount
		  
		  If compteur > 0 Then
		    Return ParametreModule.ENREGNEPEUTPASETREDETRUIT
		  Else
		    Return  "Succes"
		  End If
		  
		End Function
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validerZones(parametreDB As PostgreSQLDatabase, ByRef parametre_tri_param As WebTextField, ByRef parametre_cle_param As WebTextField, ByRef parametre_desc_fr_param As WebTextField, ByRef parametre_desc_en_param As WebTextField) As String
		  // Couper les zones à leur longeur maximum dans la BD
		  
		  parametre_cle_param.text =  Left(parametre_cle_param.text,30)
		  parametre_desc_fr_param.text = Left(parametre_desc_fr_param.text,100)
		  parametre_desc_en_param.text = Left(parametre_desc_en_param.text,100)
		  
		  // Vérification des zones obligatoires
		  If parametre_tri_param.Text = "" Then parametre_tri_param.Style = TextFieldErrorStyle
		  If parametre_cle_param.Text = "" Then parametre_cle_param.Style = TextFieldErrorStyle
		  If parametre_desc_fr_param.Text = "" Then parametre_desc_fr_param.Style = TextFieldErrorStyle
		  If parametre_desc_en_param.Text = "" Then parametre_desc_en_param.Style = TextFieldErrorStyle
		  
		  If parametre_tri_param.Style = TextFieldErrorStyle Or parametre_cle_param.Style = TextFieldErrorStyle Or parametre_desc_fr_param.Style = TextFieldErrorStyle Or parametre_desc_en_param.Style = TextFieldErrorStyle  Then
		    Return ParametreModule.ZONESOBLIGATOIRES
		  End If
		  
		  // Vérification des zones numériques
		  If parametre_tri_param.text <> "" And  IsNumeric(parametre_tri_param.text) = False Then parametre_tri_param.Style = TextFieldErrorStyle
		  
		  If parametre_tri_param.Style = TextFieldErrorStyle Then
		    Return ParametreModule.ZONEDOITETRENUMERIQUE
		  End If
		  
		  parametre_tri_param .text = str(Format( Val(parametre_tri_param .text), "0"))
		  
		  Return  "Succes"
		  
		End Function
	#tag EndMethod


	#tag Note, Name = Version
		Version 2
		Ajout de la méthode validerSuppression
		
		Version 1
	#tag EndNote


	#tag Property, Flags = &h0
		parametre_cle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_cleetr_cle As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_cleetr_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_desc_en As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_desc_fr As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_id As Integer
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_nom As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_statut As String
	#tag EndProperty

	#tag Property, Flags = &h0
		parametre_tri As Integer
	#tag EndProperty


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedBy"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockEditMode"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="lockedRowID"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_cle"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_cleetr_cle"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_cleetr_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_desc_en"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_desc_fr"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_nom"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_statut"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="parametre_tri"
			Group="Behavior"
			Type="Integer"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordAvailable"
			Group="Behavior"
			InitialValue="False"
			Type="Boolean"
		#tag EndViewProperty
		#tag ViewProperty
			Name="recordID"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="securityProfile"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="username"
			Group="Behavior"
			Type="String"
			EditorType="MultiLineEditor"
		#tag EndViewProperty
		#tag ViewProperty
			Name="uuuser_id"
			Group="Behavior"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Class
#tag EndClass

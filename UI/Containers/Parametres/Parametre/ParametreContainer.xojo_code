#tag WebPage
Begin WebContainer ParametreContainer
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   500
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   True
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "None"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   450
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebRectangle Rectangle1
      Cursor          =   0
      Enabled         =   True
      Height          =   500
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   0
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   True
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "1804472319"
      TabOrder        =   -1
      Top             =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   450
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebLabel parametre_id_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   20
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   0
      Text            =   "Id"
      TextAlign       =   0
      Top             =   446
      VerticalCenter  =   0
      Visible         =   True
      Width           =   28
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin TestWebTextField parametre_id_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   60
      Text            =   ""
      TextAlign       =   0
      Top             =   466
      Type            =   3
      VerticalCenter  =   0
      Visible         =   True
      Width           =   59
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel parametre_tri_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   47
      Text            =   "#ParametreModule.TRI"
      TextAlign       =   0
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   91
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField parametre_tri_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   ""
      TextAlign       =   0
      Top             =   65
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   30
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField parametre_desc_fr_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   5
      Text            =   ""
      TextAlign       =   0
      Top             =   122
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   431
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel parametre_desc_fr_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#ParametreModule.DESCRIPTIONFRANCAISE"
      TextAlign       =   0
      Top             =   100
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel parametre_desc_en_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#ParametreModule.DESCRIPTIONANGLAISE"
      TextAlign       =   0
      Top             =   157
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField parametre_desc_en_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   7
      Text            =   ""
      TextAlign       =   0
      Top             =   179
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   431
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel parametre_cle_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   70
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   47
      Text            =   "#ParametreModule.CLE"
      TextAlign       =   0
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   64
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebTextField parametre_cle_TextField
      AutoCapitalize  =   True
      AutoComplete    =   True
      AutoCorrect     =   True
      CueText         =   ""
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   70
      LimitText       =   0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Password        =   False
      ReadOnly        =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   3
      Text            =   ""
      TextAlign       =   0
      Top             =   65
      Type            =   0
      VerticalCenter  =   0
      Visible         =   True
      Width           =   100
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu parametre_cleetr_nom_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   10
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   9
      Text            =   ""
      Top             =   236
      VerticalCenter  =   0
      Visible         =   True
      Width           =   221
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel parametre_cleetr_nom_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#ParametreModule.TABLETRANGERE"
      TextAlign       =   0
      Top             =   214
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel parametre_cleetr_cle_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   10
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#ParametreModule.CLEETRANGERE"
      TextAlign       =   0
      Top             =   270
      VerticalCenter  =   0
      Visible         =   True
      Width           =   142
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu parametre_cleetr_cle_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   10
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   11
      Text            =   ""
      Top             =   292
      VerticalCenter  =   0
      Visible         =   True
      Width           =   431
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel parametre_statut_Label
      Cursor          =   1
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   17
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   326
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "0"
      TabOrder        =   1
      Text            =   "#ParametreModule.STATUT"
      TextAlign       =   0
      Top             =   45
      VerticalCenter  =   0
      Visible         =   True
      Width           =   105
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin GenWebPopupMenu parametre_statut_PopupMenu
      Cursor          =   0
      Enabled         =   True
      Height          =   22
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      InitialValue    =   ""
      Left            =   326
      ListIndex       =   -1
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Scope           =   0
      Style           =   "0"
      table_cle       =   ""
      table_desc_en   =   ""
      table_desc_fr   =   ""
      table_nom       =   ""
      table_tri       =   0
      TabOrder        =   9
      Text            =   ""
      Top             =   67
      VerticalCenter  =   0
      Visible         =   True
      Width           =   115
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel TLLockedByParametre
      Cursor          =   1
      Enabled         =   False
      HasFocusRing    =   True
      Height          =   15
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   251
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Multiline       =   False
      Scope           =   0
      Style           =   "1549580287"
      TabOrder        =   0
      Text            =   "Roger St-Arneault"
      TextAlign       =   0
      Top             =   466
      VerticalCenter  =   0
      Visible         =   False
      Width           =   140
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView IVLockedParametre
      AlignHorizontal =   3
      AlignVertical   =   0
      Cursor          =   0
      Enabled         =   True
      Height          =   40
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   396
      LockBottom      =   True
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   False
      LockVertical    =   False
      Picture         =   8019967
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   441
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   40
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h0
		Sub listParametreDetail()
		  // Cacher le Lock
		  IVLockedParametre.Visible = False
		  TLLockedByParametre.Visible = False
		  
		  If Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.lockEditMode Then 
		    // Paramètre 1 = WebView
		    // Paramètre 2 = Initialisation du contenu des zones
		    // Paramètre 3 = Initialisation des styles
		    // Paramètre 4 = Verrouillage des zones
		    GenControlModule.resetControls(Self, False, False, False)
		    Exit Sub
		  End If
		  
		  
		  parametre_tri_TextField.SetFocus
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, True, True, True)
		  
		  If Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre = Nil Then
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre = new ParametreClass()
		  End If
		  
		  If Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode = "Creation" Then
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode = "Modification"
		    parametre_tri_TextField.Text =str(Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.tri)
		    parametre_cle_TextField.Text =Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.cle
		    parametre_desc_fr_TextField.Text =Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.desc_fr
		    parametre_desc_en_TextField.Text =Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.desc_en
		    Exit
		  End If
		  
		  // Alimenter les listes déroulantes
		  parametre_statut_PopupMenu.loadData(Session.bdTechEol,  ParametreModule.PARAMETRETABLEName, ParametreModule.PARAMETREPREFIX, "statut")
		  parametre_cleetr_nom_PopupMenu.loadTablePrimaire(Session.bdTechEol, ParametreModule.PARAMETRETABLEName, ParametreModule.PARAMETREPREFIX)
		  
		  
		  // Mode Modification, Lire les données
		  Dim parametreRS As RecordSet = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.loadDataByField(Session.bdTechEol, ParametreModule.PARAMETRETABLEName, _
		  "parametre_id", str(Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id), "parametre_id")
		  
		  If parametreRS <> Nil Then Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.LireDonneesBD(parametreRS, ParametreModule.PARAMETREPREFIX)
		  
		  parametreRS.Close
		  parametreRS = Nil
		  
		  // Remplir la liste déroulante secondaire et la positionner à la bonne valeur
		  parametre_cleetr_cle_PopupMenu.loadCleEtrangere(Session.bdTechEol,  ParametreModule.PARAMETRETABLEName, ParametreModule.PARAMETREPREFIX,_
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_cleetr_nom, Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_cleetr_cle)
		  
		  
		  // Sauvegarde
		  sauvegardeParametreCle = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_cle
		  sauvegardeParametreTri = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_tri
		  sauvegardeParametreDescFran = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_desc_fr
		  sauvegardeParametreDescAng = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_desc_en
		  
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_id
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.cle = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_cle
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.tri = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_tri
		  
		  // Assignation des propriétés aux Contrôles
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.assignPropertiesToControls(Self, ParametreModule.PARAMETREPREFIX)
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, False, True)
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub majParametreDetail()
		  
		  
		  If Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode = "Creation" Then
		    //alimListeDeroulante
		    // Assignation des propriétés aux Contrôles
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.assignPropertiesToControls(Self, ParametreModule.PARAMETREPREFIX)
		  End If
		  
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.assignControlToProperties(Self, ParametreModule.PARAMETREPREFIX)
		  Dim messageErreur  As String = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.writeData(Session.bdTechEol, ParametreModule.PARAMETRETABLEName, ParametreModule.PARAMETREPREFIX)
		  
		  If (messageErreur <> "Succes") Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = ParametreModule.ERREUR + " " + messageErreur
		    pointeurProbMsgModal.Show
		    Exit
		  End If
		  
		  // Réafficher la liste des parametres si les valeurs ont changé
		  If (sauvegardeParametreCle <> Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_cle _
		    Or sauvegardeParametreTri <> Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_tri _
		    Or sauvegardeParametreDescFran <> Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_desc_fr _
		    Or sauvegardeParametreDescAng <> Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_desc_en _
		    Or sauvegardeParametreStatut <>  Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_statut) _
		    And Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode <> "Creation"  Then
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.populateParametre
		  End If
		  
		  sauvegardeParametreCle = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_cle
		  sauvegardeParametreTri = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_tri
		  sauvegardeParametreDescFran = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_desc_fr
		  sauvegardeParametreDescAng = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_desc_en
		  sauvegardeParametreStatut =  Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_statut
		  
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_id
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.cle = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_cle
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.tri = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_tri
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.statut =  Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.parametre_statut
		  
		  If Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode <> "Creation" Then
		    Dim ML6MajModalBox As New MajModal
		    ML6MajModalBox.Show
		  End If
		  
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode = "Modification"
		  
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Sub traiteMAJ()
		  If validationDesZonesOK = True Then
		    majParametreDetail
		    Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.edit_mode = "Modification"
		  End If
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h0
		Function validationDesZonesOK() As Boolean
		  
		  // Paramètre 1 = WebView
		  // Paramètre 2 = Initialisation du contenu des zones
		  // Paramètre 3 = Initialisation des styles
		  // Paramètre 4 = Verrouillage des zones
		  GenControlModule.resetControls(Self, False, True, False)
		  
		  Dim messageRetour as String = ""
		  
		  // Validation
		  messageRetour = Session.pointeurParametreWebDialog.ParametreSideBarDialog.sParametre.validerZones( _
		  Session.bdTechEol, _
		  parametre_tri_TextField, _
		  parametre_cle_TextField ,  _
		  parametre_desc_fr_TextField ,  _
		  parametre_desc_en_TextField)
		  
		  If messageRetour <> "Succes" Then
		    Dim pointeurProbMsgModal As ProbMsgModal = new ProbMsgModal
		    pointeurProbMsgModal.WLAlertLabel.Text = messageRetour
		    pointeurProbMsgModal.Show
		    
		    Return False
		  End If
		  
		  Return True
		  
		End Function
	#tag EndMethod


	#tag Property, Flags = &h21
		Private sauvegardeParametreCle As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeParametreDescAng As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeParametreDescFran As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeParametreStatut As String
	#tag EndProperty

	#tag Property, Flags = &h21
		Private sauvegardeParametreTri As Integer
	#tag EndProperty


#tag EndWindowCode

#tag Events parametre_desc_fr_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events parametre_desc_en_TextField
	#tag Event
		Sub KeyPressed(Details As REALbasic.KeyEvent)
		  Select Case Details.KeyCode
		  Case Details.KeyEnter
		    traiteMAJ
		  End Select
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events parametre_cleetr_nom_PopupMenu
	#tag Event
		Sub SelectionChanged()
		  // Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id = 0. Aucune information de parametre n'est associé.
		  If Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.id <> 0  And Me.ListIndex > -1 Then
		    parametre_cleetr_cle_PopupMenu.loadCleEtrangere(Session.bdTechEol, ParametreModule.PARAMETRETABLEName, ParametreModule.PARAMETREPREFIX, Me.RowTag(Me.ListIndex), "")
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior

#tag Module
Protected Module Module_MB
	#tag Constant, Name = TITLEMODAL, Type = String, Dynamic = True, Default = \"SENVION OEM Application", Scope = Protected
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"SENVION OEM Application"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"SENVION OEM Application"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Application SENVION OEM"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

#tag WebPage
Begin WebContainer DeskTopParamSubMenu
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   43
   HelpTag         =   ""
   HorizontalCenter=   0
   Index           =   -2147483648
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   True
   LockRight       =   True
   LockTop         =   True
   LockVertical    =   False
   Style           =   "0"
   TabOrder        =   0
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   1200
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
   Begin WebAnimator SlidingVertical
      Cursor          =   0
      Enabled         =   True
      Hardware2DSupported=   False
      Hardware3DSupported=   False
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      KeyFrameTime    =   0.0
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   False
      Motion          =   3
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      VerticalCenter  =   0
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView MoveUp
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   25
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   1166
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   False
      LockLeft        =   False
      LockRight       =   True
      LockTop         =   True
      LockVertical    =   False
      Picture         =   430847999
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   9
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   25
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ParamSubMenuIconTabTypEq
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   60
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Picture         =   2086432767
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   5
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   True
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ParamSubMenuLabelTypEq
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   95
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "162557951"
      TabOrder        =   0
      Text            =   "#MenuBarModule.TYPEEQUIPEMENT"
      TextAlign       =   0
      Top             =   5
      VerticalCenter  =   0
      Visible         =   True
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ParamSubMenuLabel02
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   255
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "162557951"
      TabOrder        =   0
      Text            =   "#MenuBarModule.TYPEEQUIPEMENT"
      TextAlign       =   0
      Top             =   5
      VerticalCenter  =   0
      Visible         =   False
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ParamSubMenuIconTab02
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   220
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Picture         =   2086432767
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   5
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ParamSubMenuIconTab03
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   380
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Picture         =   2086432767
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   5
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ParamSubMenuLabel03
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   415
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "162557951"
      TabOrder        =   0
      Text            =   "#MenuBarModule.TYPEEQUIPEMENT"
      TextAlign       =   0
      Top             =   5
      VerticalCenter  =   0
      Visible         =   False
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ParamSubMenuLabel04
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   575
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "162557951"
      TabOrder        =   0
      Text            =   "#MenuBarModule.TYPEEQUIPEMENT"
      TextAlign       =   0
      Top             =   5
      VerticalCenter  =   0
      Visible         =   False
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ParamSubMenuIconTab04
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   540
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Picture         =   2086432767
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   5
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebLabel ParamSubMenuLabel05
      Cursor          =   0
      Enabled         =   True
      HasFocusRing    =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   735
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Multiline       =   False
      Scope           =   0
      Style           =   "162557951"
      TabOrder        =   0
      Text            =   "#MenuBarModule.TYPEEQUIPEMENT"
      TextAlign       =   0
      Top             =   5
      VerticalCenter  =   0
      Visible         =   False
      Width           =   120
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
   Begin WebImageView ParamSubMenuIconTab05
      AlignHorizontal =   0
      AlignVertical   =   0
      Cursor          =   2
      Enabled         =   True
      Height          =   32
      HelpTag         =   ""
      HorizontalCenter=   0
      Index           =   -2147483648
      Left            =   700
      LockBottom      =   False
      LockedInPosition=   False
      LockHorizontal  =   True
      LockLeft        =   False
      LockRight       =   False
      LockTop         =   False
      LockVertical    =   True
      Picture         =   2086432767
      ProtectImage    =   True
      Scope           =   0
      Style           =   "0"
      TabOrder        =   -1
      Top             =   5
      URL             =   ""
      VerticalCenter  =   0
      Visible         =   False
      Width           =   32
      ZIndex          =   1
      _DeclareLineRendered=   False
      _HorizontalPercent=   0.0
      _IsEmbedded     =   False
      _Locked         =   False
      _NeedsRendering =   True
      _OfficialControl=   False
      _OpenEventFired =   False
      _VerticalPercent=   0.0
   End
End
#tag EndWebPage

#tag WindowCode
	#tag Method, Flags = &h21
		Private Sub appelTable(nomTable_param As String)
		  //Faire remonter le menu
		  monterMenu
		  
		  // Initialisation du numéro de ligne et appel de la modification des paramètres
		  Dim mainPageParametreWebDialog As ParametreWebDialog = new ParametreWebDialog
		  Session.pointeurParametreWebDialog = mainPageParametreWebDialog
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.table_nom = nomTable_param
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.parametreStruct.no_ligne = 0
		  Session.pointeurParametreWebDialog.ParametreSideBarDialog.populateParametre
		  Session.pointeurParametreWebDialog.Show
		  
		End Sub
	#tag EndMethod

	#tag Method, Flags = &h21
		Private Sub monterMenu()
		  Dim DeskTopMenuBarTrav As DeskTopMenuBar = DeskTopMenuBar(Self.Parent)
		  DeskTopMenuBarTrav.DtmbDeskTopMainBar.ParameterIVIcon.Enabled = True
		  DeskTopMenuBarTrav.paramMoveUp
		  
		End Sub
	#tag EndMethod


	#tag Property, Flags = &h0
		Shared menuDown As Boolean
	#tag EndProperty


#tag EndWindowCode

#tag Events SlidingVertical
	#tag Event
		Sub AnimationComplete(Tag As Variant)
		  If Not DeskTopAppSubMenu.menuDown And Not DeskTopParamSubMenu.menuDown Then
		    DeskTopMainPage.MainPageDeskTopMenuBar.height = 45
		  End If
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events MoveUp
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  monterMenu
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ParamSubMenuIconTabTypEq
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  appelTable("typeequipement")
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ParamSubMenuIconTab02
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ParamSubMenuIconTab03
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ParamSubMenuIconTab04
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag Events ParamSubMenuIconTab05
	#tag Event
		Sub MouseDown(X As Integer, Y As Integer, Details As REALbasic.MouseEvent)
		  
		End Sub
	#tag EndEvent
#tag EndEvents
#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Visible=true
		Group="ID"
		InitialValue="-2147483648"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Visible=true
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ScrollbarsVisible"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Always"
			"2 - Never"
			"3 - Vertical"
			"4 - Horizontal"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Visible=true
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Visible=true
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="300"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior

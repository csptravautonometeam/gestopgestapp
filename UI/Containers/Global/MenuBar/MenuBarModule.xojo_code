#tag Module
Protected Module MenuBarModule
	#tag Constant, Name = APP, Type = String, Dynamic = True, Default = \"Application", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Application"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Application"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Application"
	#tag EndConstant

	#tag Constant, Name = APPS, Type = String, Dynamic = True, Default = \"Applications", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Applications"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Applications"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Applications"
	#tag EndConstant

	#tag Constant, Name = CODEHEURE, Type = String, Dynamic = True, Default = \"Code Heure\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Code Heure"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code Heure"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Hour Code"
	#tag EndConstant

	#tag Constant, Name = CODEPAIEENTREE, Type = String, Dynamic = True, Default = \"Code Paie entr\xC3\xA9e\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Code Paie entr\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code Paie entr\xC3\xA9e"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Paycode entry"
	#tag EndConstant

	#tag Constant, Name = CODEPENSION, Type = String, Dynamic = True, Default = \"Code Pension\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Code Pension"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code Pension"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Perdiem Code"
	#tag EndConstant

	#tag Constant, Name = CODEREPAS, Type = String, Dynamic = True, Default = \"Code Repas\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Code Repas"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Code Repas"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Meal Code"
	#tag EndConstant

	#tag Constant, Name = DASHBOARD, Type = String, Dynamic = True, Default = \"Dashboard", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Dashboard"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Stats avanc\xC3\xA9es"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Dashboard"
	#tag EndConstant

	#tag Constant, Name = DEVELOPPEUR, Type = String, Dynamic = True, Default = \"D\xC3\xA9veloppeur", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"D\xC3\xA9veloppeur"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"D\xC3\xA9veloppeur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Developer"
	#tag EndConstant

	#tag Constant, Name = EQUIPEMENT, Type = String, Dynamic = True, Default = \"Equipment", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Equipment\t\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment\r"
	#tag EndConstant

	#tag Constant, Name = EVENEMENT, Type = String, Dynamic = True, Default = \"Event", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Event\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89v\xC3\xA9nement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Event\r"
	#tag EndConstant

	#tag Constant, Name = FORMATION, Type = String, Dynamic = True, Default = \"Formation", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Formation"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"\xC3\x89quipement"
	#tag EndConstant

	#tag Constant, Name = MAINTENANCE, Type = String, Dynamic = True, Default = \"Maintenance", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Maintenance"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Maintenance"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Maintenance"
	#tag EndConstant

	#tag Constant, Name = MENUBARMODULEPREFIX, Type = String, Dynamic = True, Default = \"\"app\"", Scope = Public
	#tag EndConstant

	#tag Constant, Name = MENUBARMODULETABLENAME, Type = String, Dynamic = True, Default = \"\"applications\"", Scope = Public
	#tag EndConstant

	#tag Constant, Name = PARAMETRE, Type = String, Dynamic = True, Default = \"Parameter", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Parameter"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Param\xC3\xA8tre"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Parameter"
	#tag EndConstant

	#tag Constant, Name = PARAMETRES, Type = String, Dynamic = True, Default = \"Parameters", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Parameters"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Param\xC3\xA8tres"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Parameters"
	#tag EndConstant

	#tag Constant, Name = PAYE, Type = String, Dynamic = True, Default = \"Paye", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Payroll\t\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Paye"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Payroll"
	#tag EndConstant

	#tag Constant, Name = PROJET, Type = String, Dynamic = True, Default = \"Projet", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Project\t\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Projet"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Project"
	#tag EndConstant

	#tag Constant, Name = QUITTER, Type = String, Dynamic = True, Default = \"Quit", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Quit"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Quitter"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Quit"
	#tag EndConstant

	#tag Constant, Name = SITE, Type = String, Dynamic = True, Default = \"Site", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Site"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Site"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Site"
	#tag EndConstant

	#tag Constant, Name = SORTIE, Type = String, Dynamic = True, Default = \"sortie", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sortie"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Sortie"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Logout"
	#tag EndConstant

	#tag Constant, Name = SOUSTRAITANT, Type = String, Dynamic = True, Default = \"Sub-contractor", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Sub-contractor"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Feuilles de temps"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Time Sheets"
	#tag EndConstant

	#tag Constant, Name = STATUT, Type = String, Dynamic = True, Default = \"Statut", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Statut"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Status"
	#tag EndConstant

	#tag Constant, Name = TIMESHEETS, Type = String, Dynamic = True, Default = \"Time Sheets", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Time Sheets\t\r\n"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Feuilles de temps"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Time Sheets"
	#tag EndConstant

	#tag Constant, Name = TYPEEQUIPEMENT, Type = String, Dynamic = True, Default = \"Type d\'\xC3\xA9quipement\r", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"Type d\'\xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Type d\'\xC3\xA9quipement"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"Equipment type"
	#tag EndConstant

	#tag Constant, Name = USER, Type = String, Dynamic = True, Default = \"User", Scope = Public
		#Tag Instance, Platform = Any, Language = Default, Definition  = \"User"
		#Tag Instance, Platform = Any, Language = fr, Definition  = \"Utilisateur"
		#Tag Instance, Platform = Any, Language = en, Definition  = \"User"
	#tag EndConstant


	#tag ViewBehavior
		#tag ViewProperty
			Name="Index"
			Visible=true
			Group="ID"
			InitialValue="-2147483648"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Left"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Name"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Super"
			Visible=true
			Group="ID"
			Type="String"
		#tag EndViewProperty
		#tag ViewProperty
			Name="Top"
			Visible=true
			Group="Position"
			InitialValue="0"
			Type="Integer"
		#tag EndViewProperty
	#tag EndViewBehavior
End Module
#tag EndModule

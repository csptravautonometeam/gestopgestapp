#tag WebPage
Begin WebPage StartUpPage
   Compatibility   =   ""
   Cursor          =   0
   Enabled         =   True
   Height          =   400
   HelpTag         =   ""
   HorizontalCenter=   0
   ImplicitInstance=   True
   Index           =   -2147483648
   IsImplicitInstance=   False
   Left            =   0
   LockBottom      =   False
   LockHorizontal  =   False
   LockLeft        =   False
   LockRight       =   False
   LockTop         =   False
   LockVertical    =   False
   MinHeight       =   400
   MinWidth        =   600
   Style           =   "None"
   TabOrder        =   0
   Title           =   "Untitled"
   Top             =   0
   VerticalCenter  =   0
   Visible         =   True
   Width           =   600
   ZIndex          =   1
   _DeclareLineRendered=   False
   _HorizontalPercent=   0.0
   _ImplicitInstance=   False
   _IsEmbedded     =   False
   _Locked         =   False
   _NeedsRendering =   True
   _OfficialControl=   False
   _OpenEventFired =   False
   _ShownEventFired=   False
   _VerticalPercent=   0.0
End
#tag EndWebPage

#tag WindowCode
	#tag Event
		Sub Shown()
		  
		  #if DebugBuild then
		    Dim dateExpiration As Date = new Date
		    dateExpiration.Year = dateExpiration.Year + 10
		    Session.Cookies.Set(Session.COOKIEUSER,"administrateur", dateExpiration, ".", "", false)
		    Session.Cookies.Set(Session.COOKIEOEMSECURITYPROFILE,"admin", dateExpiration, ".", "", false)
		  #Endif
		  
		  
		  Session.centralDatabaseName = GenControlModule.CENTRALDATABASENAMEPROD
		  If Session.environnementProp = "Test" Then  // Nous sommes en test
		    //Session.centralDatabaseName = Globals.CENTRALDATABASENAMETEST
		  End If
		  
		  Session.bdTechEol = New PostgreSQLDatabase
		  Session.bdTechEol.Host = Session.host
		  Session.bdTechEol.UserName =GenControlModule.USER
		  Session.bdTechEol.Password = GenControlModule.PASSWORD
		  Session.bdTechEol.DatabaseName =Session.centralDatabaseName
		  
		  If Not (Session.bdTechEol.Connect) Then
		    Dim ML6probMsgBox As New ProbMsgModal
		    ML6ProbMsgBox.WLAlertLabel.Text = GMTextConversion(Session.bdTechEol.ErrorMessage)
		    ML6probMsgBox.Show
		    Xojo.core.Timer.CallLater(12000, AddressOf Session.Quit)
		    Exit Sub
		  Else
		    Session.bdTechEol.SQLExecute("SET search_path TO equip, dbglobal, portail, locking, audit, public;")
		    If  Session.bdTechEol .Error Then
		      Dim ML6probMsgBox As New ProbMsgModal
		      ML6ProbMsgBox.WLAlertLabel.Text = GMTextConversion(Session.bdTechEol.ErrorMessage)
		      ML6probMsgBox.Show
		      Xojo.core.Timer.CallLater(12000, AddressOf Session.Quit)
		      Exit Sub
		    End If
		    // Mettre le user dans une variable custom Postgresql
		    Dim strUsername As String = Session.Cookies.Value(Session.COOKIEUSER)
		    Session.bdTechEol.SQLExecute("SET application.currentuser TO '" + strUsername + "';")
		    If  Session.bdTechEol .Error Then
		      Dim ML6probMsgBox As New ProbMsgModal
		      ML6ProbMsgBox.WLAlertLabel.Text = GMTextConversion(Session.bdTechEol.ErrorMessage)
		      ML6probMsgBox.Show
		      Xojo.core.Timer.CallLater(12000, AddressOf Session.Quit)
		      Exit Sub
		    End If
		    
		  End If
		  
		  // Création de l'objet user
		  Session.sUser = Nil
		  Session.sUser = New UserClass(Session.bdTechEol, "portail_user", "user")
		  
		  Dim platform As Session.PlatformType
		  platform = Session.Platform
		  // Show mobile UI if the user is on a mobile device
		  If platform = WebSession.PlatformType.iPhone Or platform = WebSession.PlatformType.iPodTouch _
		    Or platform = WebSession.PlatformType.AndroidPhone Or platform = WebSession.PlatformType.iPad Then
		    Session.mobileUser = True
		    MobileMainPage.Show
		  Else
		    DeskTopMainPage.Show
		  End If
		  
		  
		End Sub
	#tag EndEvent


	#tag Method, Flags = &h21
		Private Sub displayErrorMessage(errorMessage_param As String)
		  Dim platform As Session.PlatformType
		  platform = Session.Platform
		  
		  If platform = WebSession.PlatformType.iPhone Or platform = WebSession.PlatformType.iPodTouch _
		    Or platform = WebSession.PlatformType.AndroidPhone Or platform = WebSession.PlatformType.iPad Then
		    Dim ML6probMsgBox As New MobileProbMsgModal
		    ML6ProbMsgBox.WLAlertLabel.Text = errorMessage_param
		    ML6probMsgBox.Show
		  Else
		    Dim ML6probMsgBox As New ProbMsgModal
		    ML6ProbMsgBox.WLAlertLabel.Text = errorMessage_param
		    ML6probMsgBox.Show
		  End If
		  
		End Sub
	#tag EndMethod


#tag EndWindowCode

#tag ViewBehavior
	#tag ViewProperty
		Name="Cursor"
		Visible=true
		Group="Behavior"
		InitialValue="0"
		Type="Integer"
		EditorType="Enum"
		#tag EnumValues
			"0 - Automatic"
			"1 - Standard Pointer"
			"2 - Finger Pointer"
			"3 - IBeam"
			"4 - Wait"
			"5 - Help"
			"6 - Arrow All Directions"
			"7 - Arrow North"
			"8 - Arrow South"
			"9 - Arrow East"
			"10 - Arrow West"
			"11 - Arrow Northeast"
			"12 - Arrow Northwest"
			"13 - Arrow Southeast"
			"14 - Arrow Southwest"
			"15 - Splitter East West"
			"16 - Splitter North South"
			"17 - Progress"
			"18 - No Drop"
			"19 - Not Allowed"
			"20 - Vertical IBeam"
			"21 - Crosshair"
		#tag EndEnumValues
	#tag EndViewProperty
	#tag ViewProperty
		Name="Enabled"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Height"
		Visible=true
		Group="Behavior"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HelpTag"
		Visible=true
		Group="Behavior"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="HorizontalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Index"
		Group="ID"
		InitialValue="-2147483648 "
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="IsImplicitInstance"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Left"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockBottom"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockHorizontal"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockLeft"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockRight"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockTop"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="LockVertical"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinHeight"
		Visible=true
		Group="Behavior"
		InitialValue="400"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="MinWidth"
		Visible=true
		Group="Behavior"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Name"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Super"
		Visible=true
		Group="ID"
		Type="String"
	#tag EndViewProperty
	#tag ViewProperty
		Name="TabOrder"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Title"
		Visible=true
		Group="Behavior"
		InitialValue="Untitled"
		Type="String"
		EditorType="MultiLineEditor"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Top"
		Group="Position"
		InitialValue="0"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="VerticalCenter"
		Group="Behavior"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Visible"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="Width"
		Visible=true
		Group="Behavior"
		InitialValue="600"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="ZIndex"
		Group="Behavior"
		InitialValue="1"
		Type="Integer"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_DeclareLineRendered"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_HorizontalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ImplicitInstance"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_IsEmbedded"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_Locked"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_NeedsRendering"
		Group="Behavior"
		InitialValue="True"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OfficialControl"
		Group="Behavior"
		InitialValue="False"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_OpenEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_ShownEventFired"
		Group="Behavior"
		Type="Boolean"
	#tag EndViewProperty
	#tag ViewProperty
		Name="_VerticalPercent"
		Group="Behavior"
		Type="Double"
	#tag EndViewProperty
#tag EndViewBehavior
